{
  flake,
  lib,
  stdenv,
  perl,
  fetchFromGitHub,
  fetchpatch,
  makeWrapper,
  nix-update-script,
  testers,
  cowsay,
}:
stdenv.mkDerivation rec {
  pname = "xfce-winxp-tc";
  version = "3.7.0";

  outputs = ["out"];

  src = flake.inputs.xfce-winxp-tc;

  nativeBuildInputs = [makeWrapper];
  buildInputs = [perl];

  postInstall = ''
    wrapProgram $out/bin/cowsay \
      --suffix COWPATH : $out/share/cowsay/cows
  '';

  makeFlags = [
    "prefix=${placeholder "out"}"
  ];

  passthru = {
    updateScript = nix-update-script {};
    tests.version = testers.testVersion {
      package = cowsay;
      command = "cowsay --version";
    };
  };

  meta = with lib; {
    description = "A program which generates ASCII pictures of a cow with a message";
    homepage = "https://cowsay.diamonds";
    changelog = "https://github.com/cowsay-org/cowsay/releases/tag/v${version}";
    license = licenses.gpl3Only;
    platforms = platforms.all;
    maintainers = with maintainers; [rob anthonyroussel];
  };
}
