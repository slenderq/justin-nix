{
  description = "Justin's NixOS / nix-darwin configuration";

  inputs = {
    # Principle inputs
    # nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    nix-darwin.url = "github:lnl7/nix-darwin";
    nix-darwin.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager/release-24.11";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";

    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    agenix.url = "github:ryantm/agenix";
    # I do not want an experimental build for my encryption.
    agenix.inputs.nixpkgs.follows = "nixpkgs";

    musnix = {url = "github:musnix/musnix";};

    nix-serve-ng.url = "github:aristanetworks/nix-serve-ng";

    nixpkgs-match.url = "github:srid/nixpkgs-match";
    nuenv.url = "github:DeterminateSystems/nuenv";
    nixd.url = "github:nix-community/nixd";
    nixci.url = "github:srid/nixci";

    # FIXME: get rid of this...
    coc-rust-analyzer.url = "github:fannheyward/coc-rust-analyzer";
    coc-rust-analyzer.flake = false;

    # CI server
    jenkins-nix-ci.url = "github:juspay/jenkins-nix-ci";

    # Vim plugins not in nixpkgs
    gen-nvim.url = "github:David-Kunz/gen.nvim";
    gen-nvim.flake = false;

    comfortable-motion.url = "github:yuttie/comfortable-motion.vim";
    comfortable-motion.flake = false;

    numbers.url = "github:myusuf3/numbers.vim";
    numbers.flake = false;

    # let g:numbers_exclude = ['tagbar', 'gundo', 'minibufexpl', 'nerdtree']
    vim-scimark.url = "github:mipmip/vim-scimark";
    vim-scimark.flake = false;
    cellular-automaton.url = "github:eandrju/cellular-automaton.nvim";
    cellular-automaton.flake = false;

    ollama-telebot.url = "gitlab:slenderq/ollama-telebot";
    ollama-telebot.inputs.nixpkgs.follows = "nixpkgs";

    xfce-winxp-tc.url = "github:rozniak/xfce-winxp-tc";
    xfce-winxp-tc.flake = false;

    # selfhostblocks.url = "github:ibizaman/selfhostblocks";
    selfhostblocks.url = "github:slenderq/selfhostblocks";

    emojinvim.url = "github:Allaman/emoji.nvim";
    emojinvim.flake = false;

    telescope-emoji-nvim = {
      url = "github:xiyaowong/telescope-emoji.nvim";
      flake = false;
    };

    gruvbox.url = "github:ellisonleao/gruvbox.nvim";
    gruvbox.flake = false;

    nix-on-droid = {
      url = "github:nix-community/nix-on-droid/release-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    codecompanion = {
      url = "github:olimorris/codecompanion.nvim";
      flake = false;
    };

    hyprland.url = "github:hyprwm/Hyprland";

    flake-utils.url = "github:numtide/flake-utils";
    pre-commit-hooks.url = "github:cachix/git-hooks.nix";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
    nixpkgs-unstable,
    home-manager,
    nixos-generators,
    agenix,
    musnix,
    nixos-hardware,
    nix-serve-ng,
    nix-on-droid,
    codecompanion,
    gruvbox,
    telescope-emoji-nvim,
    emojinvim,
    ollama-telebot,
    pre-commit-hooks,
    ...
  } @ attrs: let
    unfree = ["steam" "unrar" "_1password-cli" "_1password-gui"];
    pkgs = import nixpkgs {
      inherit system;
      hostPlatform = "x86_64-linux";

      config = {
        allowUnfree = true;
        allowUnsupportedSystem = true;
        # cudaSupport = true;
        permittedInsecurePackages = [
          "python3.12-youtube-dl-2021.12.17"
        ];
        allowUnfreePredicate = (
          pkg:
            builtins.elem (pkg.pname or (builtins.parseDrvName pkg.name).name) unfree
        );
        allowed-uris = ["https://downloads.1password.com/linux/tar/stable/x86_64" "https://statics.teams.cdn.office.net/production-osx/1.6.00.4464" "https://developer.download.nvidia.com" "https://python.org" "https://download.nvidia.com" "https://tarballs.nixos.org" "https://bugsfiles.kde.org" "https://git.savannah.gnu.org"];
      };
    };

    system = "x86_64-linux"; # for now don't worry about the cross stuff
  in {
    checks = {
      "${system}" = {
        # https://devenv.sh/?q=pre-commit.hooks
        pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            alejandra.enable = true;
            commitizen.enable = true;
          };
        };
      };
    };

    devShells = {
      "${system}".default = pkgs.mkShell {
        inherit (self.checks.${system}.pre-commit-check) shellHook;
        buildInputs = self.checks.${system}.pre-commit-check.enabledPackages;
      };
    };

    # Configurations for Linux (NixOS) systems
    nixosConfigurations = {
      nixvirt = nixpkgs.lib.nixosSystem {
        inherit pkgs;
        modules = [
          agenix.nixosModules.default
          "${toString nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-base.nix"
          nixos-generators.nixosModules.all-formats
          ./nixos/default.nix # Defined in nixos/default.nix
          ./systems/nixvirt.nix
          ./nixos/gui/xfce4-basic.nix
          ./nixos/borgclient.nix
        ];
        specialArgs = attrs;
      };
      nixpad = nixpkgs.lib.nixosSystem {
        inherit pkgs;

        modules = [
          ./nixos/default.nix # Defined in nixos/default.nix
          agenix.nixosModules.default
          home-manager.nixosModules.default
          ./systems/nixpad.nix
        ];
        specialArgs = attrs;
      };
      nixgaming = nixpkgs.lib.nixosSystem {
        inherit pkgs;
        modules = [
          ./nixos/default.nix
          agenix.nixosModules.default
          home-manager.nixosModules.default
          nixos-hardware.nixosModules.common-cpu-intel
          nixos-hardware.nixosModules.common-pc-laptop-ssd
          nixos-hardware.nixosModules.common-pc-laptop
          ./systems/nixgaming.nix
          ./nixos/gaming.nix
          ./nixos/media.nix
          ./nixos/gui/picom.nix
          ./nixos/nvidia.nix
          ./nixos/gui/xfce4-basic.nix
          ./nixos/iso-builder.nix
          #./nixos/pulseaudio.nix
        ];
        specialArgs = attrs;
      };
      nixmusic = nixpkgs.lib.nixosSystem {
        inherit pkgs;
        modules = [
          ./nixos/default.nix
          agenix.nixosModules.default
          home-manager.nixosModules.default
          musnix.nixosModules.musnix
          ./nixos/media.nix
          ./nixos/nixos-music.nix
          ./nixos/gui/i3.nix
          ./nixos/comms.nix
          agenix.nixosModules.default
          ./nixos/gui/picom.nix
          ./nixos/docker.nix
          ./nixos/office.nix
          ./nixos/books.nix
          ./systems/nixmusic.nix
          ./nixos/noise-suppression.nix
        ];
        specialArgs = attrs;
      };
      nixbook = nixpkgs.lib.nixosSystem {
        inherit pkgs;
        modules = [
          ./nixos/default.nix # Defined in nixos/default.nix
          agenix.nixosModules.default
          home-manager.nixosModules.default
          # https://github.com/NixOS/nixos-hardware/blob/master/flake.nix
          nixos-hardware.nixosModules.common-cpu-intel
          nixos-hardware.nixosModules.common-pc-laptop-ssd
          nixos-hardware.nixosModules.common-pc-laptop
          ./nixos/media.nix
          ./nixos/gui/i3.nix
          # ./nixos/books.nix
          ./systems/nixbook.nix
          ./nixos/gui/picom.nix
          # ./nixos/office.nix too many pkgs
          ./nixos/comms.nix
          ./nixos/iso-builder.nix
          ./nixos/inmemory-cache.nix
        ];

        specialArgs = attrs;
      };
      nixbase = nixpkgs.lib.nixosSystem {
        inherit pkgs;
        modules = [
          ./nixos/default.nix # Defined in nixos/default.nix
          nix-serve-ng.nixosModules.default
          agenix.nixosModules.default
          # selfhostblocks.nixosModules.default
          ./systems/nixbase.nix
          ./nixos/nvidia.nix
          ./nixos/media.nix
          ./nixos/syn213.nix
          ./nixos/gaming.nix
          ./nixos/server/harden.nix
          ./nixos/docker.nix
          ./nixos/gui/i3.nix
          ./nixos/gui/picom.nix
          # ./nixos/ollama_telebot.nix
          ./nixos/borg-server.nix
          ./nixos/iso-builder.nix
          ./nixos/cache-server.nix
          # ./nixos/immich.nix
          # ./nixos/spark.nix probably not the right thing
          #./nixos/nightly.nix
        ];
        specialArgs = attrs;
      };
      nixexit = nixpkgs.lib.nixosSystem {
        inherit pkgs;
        modules = [
          ./nixos/default.nix # Defined in nixos/default.nix
          agenix.nixosModules.default
          home-manager.nixosModules.default
          ./systems/nixexit.nix
          ./nixos/server/harden.nix
          ./nixos/docker.nix
          ./nixos/gui/picom.nix
          ./nixos/gui/i3.nix
          ./nixos/tailscale-exit-node.nix
        ];
        specialArgs = attrs;
      };
    };

    # Standalone home-manager configuration entrypoint
    # Available through 'home-manager --flake .#your-username@your-hostname'
    homeConfigurations = {
      "default" = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages.x86_64-linux; # Home-manager requires 'pkgs' instance
        modules = [
          # > Our main home-manager configuration file <
          ./home/default.nix
        ];
      };
    };

    nixOnDroidConfigurations.default = nix-on-droid.lib.nixOnDroidConfiguration {
      modules = [
        ./home/nix-on-droid.nix
      ];
    };
  };
  nixConfig = {
    ########allowed-uris = ["python.org" "download.nvidia.com" "pypi.org" "bugsfiles.kde.org" "git.savannah.gnu.org"];
    trusted-public-keys = ["cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=" "cache.garnix.io:CTFPyKSLcx5RMJKfLo5EEPUObbA78b0YQ2DTCJXqr9g="];
    # substituters = "https://nammayatri.cachix.org";
    substituters = ["https://cache.nixos.org" "https://nixbase.hedgehog-augmented.ts.net"];
    builders-use-substitutes = true;
    allowUnfree = true;
    sandbox = false;
  };
}
