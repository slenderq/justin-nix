# Justin NixOS Config

NixOS full config that includes:
- Non NixOS Home Manager
- Linux NixOS modules.
- Nix on Droid Configuration 

Forked from: https://github.com/srid/nixos-config

Changes in the Fork:
- My personal development tools via Home Manager 
- Added my personal NixOS Modules
    - My i3 based rice 
    - My personal favorite open source gui apps.
    - Linux Audio Setup in Nix
    - Linux Gaming Configuration, with emulators and Steam.
- Changed secrets management to use agenix. 

## Setup 
To use this repository as base configuration for your new machine running:

### Try it out! 

```
export NIX_CONFIG="experimental-features = nix-command flakes"
nix develop

# Add Dot Files.
home-manager switch --flake .#default

# NOTE: the above command will fail on existing files rather than overrite.
#  -b EXT            Move existing files to new path rather than fail.

```

### Revert
```
home-manager uninstall

```


### NixOS Linux

> [!TIP] 
> For a general tutorial, see https://nixos.asia/en/tutorial/nixos-install

#### Install NixOS
  - Linux: https://nixos.org/download.html#nixos-iso
    - X1 Carbon: https://srid.ca/x1c7-install
  - Windows (via WSL): https://github.com/nix-community/NixOS-WSL

#### Bootstrap on NixOS

```
nix-shell -p git openssh neovim fish
git clone https://gitlab.com/slenderq/justin-nix.git # https to avoid ssh key
cd justin-nix
cp /etc/nixos/hardware-configuration.nix ./systems/hardware/<hostname>-hw.nix
git add ./systems
git commit --author="Bootstrap commit <your.email@example.com>"
nixos-rebuild switch --flake .#nixinstall --use-remote-sudo --extra-experimental-features nix-command --extra-experimental-features flakes
```

- Clone this repo anywhere
- Edit `flake.nix` to use your system hostname as a key of the `nixosConfigurations` set
- Edit `users/config.nix` to contain your users
- Run `nix run`. That's it. Re-open your terminal.

### macOS

- [Install Nix](https://haskell.flake.page/nix)
- Install [nix-darwin](https://github.com/LnL7/nix-darwin) 
    - This will create a `~/.nixpkgs/darwin-configuration.nix`, but we do not need that. 
- Clone this repo anywhere
- Edit `flake.nix` to use your system hostname as a key of the `darwinConfigurations` set
- Edit `users/config.nix` to contain your users Run `nix run`.[^cleanup] That's it. Re-open your terminal.
[^cleanup]: You might have to `rm -rf /etc/nix/nix.conf`, so our flake.nix can do its thing.

## Architecture

Start from `flake.nix` (see [Flakes](https://nixos.wiki/wiki/Flakes)). [`flake-parts`](https://flake.parts/) is used as the module system. 

### Directory layout 

- `home`: home-manager config (shared between Linux and macOS)
- `nixos`: nixos modules for Linux
- `nix-darwin`: nix-darwin modules for macOS
- `users`: user information
- `secrets.yaml` (and `.sops.yaml`):  sops-nix secrets
- `systems`: top-level configuration.nix('ish) for various systems

## Tips

- To update NixOS (and other inputs) run `nix flake update`
  - You may also update a subset of inputs, e.g.
      ```sh
      nix flake lock --update-input nixpkgs --update-input darwin --update-input home-manager
      # Or, `nix run .#update`
      ```
- To free up disk space,
    ```sh-session
    sudo nix-env -p /nix/var/nix/profiles/system --delete-generations +2
    sudo nixos-rebuild boot
    ```
- To autoformat the project tree using nixpkgs-fmt, run `nix fmt`.
- To build all flake outputs (locally or in CI), run `nix run nixpkgs#nixci`
## Secrets with `agenix`
to create edit secrets follow this guide: https://github.com/ryantm/agenix

### Edit secrets.nix

add the line
```
  "nixos/secrets/secret1.age".publicKeys = all_keys;
```

### Create new secret

- create a secret file
```
agenix -e nixos/secrets/secret1.age
```
- 

add to `nixos/inject-secrets.nix` some varient of the following nix:
```
age.secrets.wireless-networks = {
    file = ./secrets/wireless-networks.env.age;
    name = "wireless-networks.env";
    owner = "${people.myself}";
    group = "users";
  };

```

### Decrypt Secrets on Boot
In this setup we use our user ssh keys to encrypt our secrets.

Its possible that these keys will not be around at the right time on boot.

When you get your running system
```
agenix -r
```
This will also use the generated hostkey at `/etc/ssh/ssh_host_ed25519_key` to encrypt the secretes so there is a way for the system root to encrypt them

