# https://wiki.archlinux.org/title/ASUS_C302
{
  config,
  pkgs,
  lib,
  ...
}: let
  myCustomLayout = pkgs.writeText "xkb-layout" ''
    keycode   9 = Escape NoSymbol Escape
    keycode  22 = BackSpace BackSpace BackSpace BackSpace Delete NoSymbol Delete
    keycode  37 = Control_L NoSymbol Control_L
    keycode  50 = Shift_L NoSymbol Shift_L
    keycode  66 = Caps_Lock NoSymbol Caps_Lock
    keycode  67 = Home F1 Home F1 F1 F1 XF86Switch_VT_1
    keycode  68 = End F2 End F2 F2 F2 XF86Switch_VT_2
    keycode  69 = Prior F3 Prior F3 F3 F3 XF86Switch_VT_3
    keycode  70 = Next F4 Next F4 F4 F4 XF86Switch_VT_4
    keycode  71 = Delete F5 Delete F5 F5 F5 XF86Switch_VT_5
    keycode  72 = XF86MonBrightnessDown F6 XF86MonBrightnessDown F6 F6 F6 XF86Switch_VT_6
    keycode  73 = XF86MonBrightnessUp F7 XF86MonBrightnessUp F7 F7 F7 XF86Switch_VT_7
    keycode  74 = XF86AudioMute F8 XF86AudioMute F8 F8 F8 XF86Switch_VT_8
    keycode  75 = XF86AudioLowerVolume F9 XF86AudioLowerVolume F9 F9 F9 XF86Switch_VT_9
    keycode  76 = XF86AudioRaiseVolume F10 XF86AudioRaiseVolume F10 F10 F10 XF86Switch_VT_10
    keycode 111 = Up Up Up Up Prior Prior
    keycode 112 = Prior NoSymbol Prior
    keycode 113 = Left Left Left Left Home Home
    keycode 114 = Right Right Right Right End End
    keycode 115 = End NoSymbol End
    keycode 116 = Down Down Down Down Next Next
    keycode 117 = Next NoSymbol Next
    keycode 118 = Insert NoSymbol Insert
    keycode 119 = Delete NoSymbol Delete
    keycode 124 = XF86PowerOff NoSymbol XF86PowerOff
    keycode 167 = XF86Forward NoSymbol XF86Forward
    keycode 182 = XF86Close NoSymbol XF86Close
    keycode 191 = XF86ScreenSaver NoSymbol XF86ScreenSaver
  '';
in {
  imports = [
    ./hardware/nixbook-hw.nix
  ];

  # LVM defined in:
  # ./disko/lvm.nix
  networking.hostName = "nixbook"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sdc"; # or "nodev" for efi only

  powerManagement.cpuFreqGovernor = "powersave";

  services.thermald = {
    enable = true;
  };
  services.tlp.enable = true;

  # Set your time zone.
  time.timeZone = "America/Edmonton";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_CA.UTF-8";

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
    model = "chromebook";
    # options = "ctrl:nocaps,altwin:meta_win,shift:bothcapslock_cancel";
  };

  services.logrotate.checkConfig = false;

  users.users.justin = {
    isNormalUser = true;
    description = "justin";
    extraGroups = ["networkmanager" "wheel"];
    packages = with pkgs; [
      firefox
    ];
  };

  # Enable automatic login for the user.
  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "justin";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #  wget
  ];

  system.stateVersion = "23.11"; # Did you read the comment?

  home-manager.users.justin = {
    programs.kitty = {
      # browse themes
      # kitty +kitten themes
      # theme = "Japanesque";
      theme = "Glacier";
      # theme = "Fun Forrest";
    };
  };

  services.touchegg.enable = true;

  services.libinput = {
    enable = true;
    # touchpad.enable = true;
    # touchpad.naturalScroll = true;
    touchpad = {
      disableWhileTyping = true;
      tapping = true;
      # naturalScrolling = true;
      scrollMethod = "twofinger";
    };
  };

  home-manager.users.justin = {
    home.file.".background-image".source = ../nixos/gui/backgrounds/david-becker-slate-unsplash.jpg;
    imports = [
      ../home/libinput-gestures.nix
    ];
  };

  services.xserver = {
    modules = [pkgs.xf86_input_wacom];
    wacom.enable = true;
  };

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  services.xserver.displayManager.sessionCommands = "${pkgs.xorg.xmodmap}/bin/xmodmap ${myCustomLayout}";

  boot.extraModprobeConfig = lib.mkMerge [
    # Needed, for the nau8825 kernel sound modules to initialize
    "blacklist snd_hda_intel"
    "options tpm_tis interrupts=0"
  ];
}
