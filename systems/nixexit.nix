# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    # /etc/nixos/hardware-configuration.nix
    ./hardware/nixexit-hw.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixexit"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  # networking.networkmanager.enable = true;

  services.throttled.enable = true;

  # Enable network manager applet
  programs.nm-applet.enable = true;

  # Set your time zone.
  time.timeZone = "America/Edmonton";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_CA.UTF-8";

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # Enable the LXQT Desktop Environment.
  # services.xserver.displayManager.lightdm.enable = true;
  # services.xserver.desktopManager.lxqt.enable = true;

  # Configure keymap in X11
  # services.xserver = {
  #   layout = "us";
  #   xkbVariant = "";
  # };

  # Enable sound with pipewire.
  # sound.enable = true;
  # hardware.pulseaudio.enable = false;
  # security.rtkit.enable = true;
  # services.pipewire = {
  #  enable = true;
  # alsa.enable = true;
  # alsa.support32Bit = true;
  # pulse.enable = true;
  # # If you want to use JACK applications, uncomment this
  #jack.enable = true;

  # use the example session manager (no others are packaged yet so this is enabled by default,
  # no need to redefine it in your config for now)
  #media-session.enable = true;
  # };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.justin = {
    isNormalUser = true;
    description = "justin";
    extraGroups = ["networkmanager" "wheel"];
    packages = with pkgs; [
      firefox
      #  thunderbird
    ];
  };

  home-manager.users.justin = {
    programs.kitty = {
      # browse themes
      # kitty +kitten themes
      #theme = "Japanesque";
      theme = "Glacier";
      # theme = "Fun Forrest";
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #  wget
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

  # need to make sure /export is owned by nobody:nogroup
  #  https://nixos.wiki/wiki/NFS

  services.auto-cpufreq.enable = true;
  services.auto-cpufreq.settings = {
    battery = {
      governor = "powersave";
      turbo = "never";
    };
    charger = {
      governor = "powersave";
      turbo = "never";
    };
  };
  powerManagement.cpuFreqGovernor = "powersave";

  services.thermald = {
    enable = true;
  };
  services.tlp.enable = true;

  users.users."justin".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAi0v4eHGCPZUAfPLWYOMVzRenZO3HeBnUrfpq1EYs32 justin@nixbase" # build key
    # note: ssh-copy-id will add user@your-machine after the public key
    # but we can remove the "@your-machine" part

    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCLtmwmO7/vtqnu2erHWv0qwfb844N6vaOnIKDvxV0YTzMPG+BF9oJjaiA5EYYvcfmoeciwxbkPJZSvwRC52v/hzibVgA2dl557QOq7weDtXfKw/69GnoZugZBg4Vm5b+Vf16+OJn561d4zTRjjhGOrLy+hkEn5NTneI4qzh/LZsQ/AWAiGzLswpkqrnvzt7nDBSlKKPikWPNeVH3B93b6YH/xqiyBgR6GvpH6TOElqBWXj8J22M1Gavlpq0CVgdMN7uur343T7XdnJYCbt3bg9de6LaUciJu7kof2UV09yv9A9vNKJPbJl0yoSGQLvb45qUm4nkdJC4JtpE/gUgLIOk3N7EbjeGn43R8tm1ZT4ojUb5Pr2rO6qcyqf24Q770EFBfKeoyZjRaLhsJZC+PT1/qiZo+sWGE2vnm4C3Lg9zScN/ay3bQQGMKLTMFEOEITzVvjQk8q8K02jI2Km1s8EbcDsJq3i9prP7lBB33kmf58EXntTxT4a0GRgI+yifoU= justin@nixbase"
  ];

  home-manager.users.justin = {
    home.file.".background-image".source = ../nixos/gui/backgrounds/ishfaq-ahmed-macbook-unsplash.jpg;
  };
}
