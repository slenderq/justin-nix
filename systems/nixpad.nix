# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  pkgs,
  lib,
  musnix,
  nixos-hardware,
  ...
}: {
  specialisation = {
    i3-picom.configuration = {
      home-manager.users.justin = {
        home.file.".background-image".source = ../nixos/gui/backgrounds/jimmy-dean-fruit-unsplash.jpg;
      };
      imports = [
        ../nixos/gui/picom.nix
        ../nixos/pipewire.nix
      ];
    };
    work.configuration = {
      imports = [
        ../nixos/gui/picom.nix
        ../nixos/work.nix
        ../nixos/pipewire.nix
      ];
    };
    gaming.configuration = {
      home-manager.users.justin = {
        home.file.".background-image".source = ../nixos/gui/backgrounds/lorenzo-herrera-light-retro-unsplash.jpg;
      };
      imports = [
        ../nixos/gui/picom.nix
        ../nixos/gaming.nix
        ../nixos/pipewire.nix
      ];
    };
    music = {
      inheritParentConfig = false;
      configuration = {
        home-manager.users.justin = {
          home.file.".background-image".source = ../nixos/gui/backgrounds/panagiotis-falcos-keyboard-unsplash.jpg;
        };
        imports = [
          musnix.nixosModules.musnix
          ../nixos/default.nix
          ../nixos/media.nix
          ../nixos/nixos-music.nix
          ./hardware/nixpad-hw2.nix
          ../nixos/gui/i3.nix
        ];
      };
    };
  };

  # TODO: make this a "defaults_nixpad" file/var so we can add them to the specialisation
  # that inheritParentConfig = false;
  imports = [
    ../nixos/borgclient.nix
    ../nixos/redteam.nix
    ../nixos/docker.nix
    ../nixos/office.nix
    ../nixos/books.nix
    ../nixos/media.nix
    # ../nixos/iso-builder.nix
    ../nixos/gui/i3.nix
    ../nixos/comms.nix
    ./hardware/nixpad-hw2.nix
    nixos-hardware.nixosModules.lenovo-thinkpad-t480s
  ];

  services.throttled.enable = true;

  services.fstrim = {
    enable = true;
    interval = "daily";
  };

  boot.extraModprobeConfig = lib.mkMerge [
    # idle audio card after one second
    "options snd_hda_intel power_save=120"
    # enable wifi power saving (keep uapsd off to maintain low latencies)
    #"options iwlwifi power_save=1 uapsd_disable=1"
    # try this if this does not work
    # options mac80211 beacon_loss_count=1000 probe_wait_ms=75
    # options ath9k debug=0xffffffff btcoex_enable=0 ps_enable=0 use_msi=0
  ];

  # https://github.com/georgewhewell/nixos-host/blob/master/profiles/thinkpad.nix
  boot.initrd = {
    supportedFilesystems = ["nfs"];
    kernelModules = ["nfs" "thinkpad-acpi"];
  };

  boot.kernelParams = [
    "msr.allow_writes=on"
    "cpuidle.governor=teo"
  ];
  services.upower.enable = true;

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # boot.loader.efi.efiSysMountPoint = "/boot/efi";

  services.libinput.touchpad.middleEmulation = true;

  hardware.trackpoint = {
    enable = true;
    emulateWheel = true;
    speed = 250;
    sensitivity = 100;
  };

  services.logind.lidSwitch = "ignore";
  services.logind.lidSwitchDocked = "ignore";
  services.logind.lidSwitchExternalPower = "ignore";
  services.logind.extraConfig = "HandleLidSwitch=ignore";
  services.upower.ignoreLid = true;
  services.xserver.displayManager.gdm.autoSuspend = false;
  # services.logind = {
  #   lidSwitch = "ignore";
  #   lidSwitchExternalPower = "lock";
  #   extraConfig = ''
  #     # transition from suspend to hibernate after 1h
  #     HibernateDelaySec=3600
  #   '';
  # };

  hardware.bluetooth = {
    enable = true;
    powerOnBoot = false;
  };

  #https://nixos.wiki/wiki/IBus
  # TODO: emoji support lol
  # sound.enable = true;

  networking.hostName = "nixpad"; # Define your hostname.

  # Set your time zone.

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.justin = {
    isNormalUser = true;
    description = "justin";
    shell = pkgs.fish;
    packages = with pkgs; [];
    extraGroups = ["docker" "wheel" "sudo"];
  };

  home-manager.users.justin = {
    programs.kitty = {
      # browse themes
      # kitty +kitten themes
      theme = "Japanesque";
      #theme = "Glacier";
      # theme = "Fun Forrest";
    };
  };

  services.actkbd = {
    enable = true;
    bindings = [
      {
        keys = [113];
        events = ["key"];
        command = "/run/current-system/sw/bin/runuser -l justin -c 'amixer -q set Master toggle'";
      }
      {
        keys = [114];
        events = ["key"];
        command = "/run/current-system/sw/bin/runuser -l justin -c 'amixer -q set Master 5%- unmute'";
      }
      {
        keys = [115];
        events = ["key"];
        command = "/run/current-system/sw/bin/runuser -l justin -c 'amixer -q set Master 5%+ unmute'";
      }
    ];
  };

  # pkgs.config.packageOverrides = pkgs: {
  #   vaapiIntel = pkgs.vaapiIntel.override {enableHybridCodec = true;};
  # };
  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vpl-gpu-rt
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  # we are a nixos system we don't have that much write
  fileSystems."/home/justin/.cache" = {
    device = "none";
    fsType = "tmpfs";
    options = ["defaults" "size=25%" "mode=755" "uid=justin"];
  };

  environment.sessionVariables = {
    XDG_HOME_DIR = "$HOME";
    XDG_CACHE_HOME = "$HOME/.cache";
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?
  # Keyboard event codes for custom buttons
  # Event code 113 (KEY_MUTE)
  # Event code 114 (KEY_VOLUMEDOWN)
  # Event code 115 (KEY_VOLUMEUP)
  # Event code 120 (KEY_SCALE)
  # Event code 140 (KEY_CALC)
  # Event code 142 (KEY_SLEEP)
  # Event code 144 (KEY_FILE)
  # Event code 148 (KEY_PROG1)
  # Event code 149 (KEY_PROG2)
  # Event code 152 (KEY_SCREENLOCK)
  # Event code 156 (KEY_BOOKMARKS)
  # Event code 158 (KEY_BACK)
  # Event code 171 (KEY_CONFIG)
  # Event code 173 (KEY_REFRESH)
  # Event code 190 (KEY_F20)
  # Event code 191 (KEY_F21)
  # Event code 194 (KEY_F24)
  # Event code 202 (KEY_PROG3)
  # Event code 205 (KEY_SUSPEND)
  # Event code 212 (KEY_CAMERA)
  # Event code 216 (KEY_CHAT)
  # Event code 217 (KEY_SEARCH)
  # Event code 218 (KEY_CONNECT)
  # Event code 223 (KEY_CANCEL)
  # Event code 224 (KEY_BRIGHTNESSDOWN)
  # Event code 225 (KEY_BRIGHTNESSUP)
  # Event code 227 (KEY_SWITCHVIDEOMODE)
  # Event code 228 (KEY_KBDILLUMTOGGLE)
  # Event code 236 (KEY_BATTERY)
  # Event code 237 (KEY_BLUETOOTH)
  # Event code 238 (KEY_WLAN)
  # Event code 240 (KEY_UNKNOWN)
  # Event code 372 (KEY_ZOOM)
  # Event code 374 (KEY_KEYBOARD)
  # Event code 466 (KEY_FN_F1)
  # Event code 475 (KEY_FN_F10)
  # Event code 476 (KEY_FN_F11)
  # Event code 582 (KEY_VOICECOMMAND)
  # Event code 592 (KEY_BRIGHTNESS_MIN)
  services.udev.extraRules = ''
        # Your rule goes here
        # Rules for Oryx web flashing and live training
    KERNEL=="hidraw*", ATTRS{idVendor}=="16c0", MODE="0664", GROUP="plugdev"
    KERNEL=="hidraw*", ATTRS{idVendor}=="3297", MODE="0664", GROUP="plugdev"

    # Legacy rules for live training over webusb (Not needed for firmware v21+)
      # Rule for all ZSA keyboards
      SUBSYSTEM=="usb", ATTR{idVendor}=="3297", GROUP="plugdev"
      # Rule for the Moonlander
      SUBSYSTEM=="usb", ATTR{idVendor}=="3297", ATTR{idProduct}=="1969", GROUP="plugdev"
      # Rule for the Ergodox EZ
      SUBSYSTEM=="usb", ATTR{idVendor}=="feed", ATTR{idProduct}=="1307", GROUP="plugdev"
      # Rule for the Planck EZ
      SUBSYSTEM=="usb", ATTR{idVendor}=="feed", ATTR{idProduct}=="6060", GROUP="plugdev"

    # Wally Flashing rules for the Ergodox EZ
    ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789B]?", ENV{ID_MM_DEVICE_IGNORE}="1"
    ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789A]?", ENV{MTP_NO_PROBE}="1"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789ABCD]?", MODE:="0666"
    KERNEL=="ttyACM*", ATTRS{idVendor}=="16c0", ATTRS{idProduct}=="04[789B]?", MODE:="0666"

    # Keymapp / Wally Flashing rules for the Moonlander and Planck EZ
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="df11", MODE:="0666", SYMLINK+="stm32_dfu"
    # Keymapp Flashing rules for the Voyager
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="3297", MODE:="0666", SYMLINK+="ignition_dfu"
  '';
}
