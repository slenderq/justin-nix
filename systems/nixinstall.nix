{
  config,
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    (writeShellApplication {
      name = "nix-install-justin";
      runtimeInputs = [];
      text = ''
        set -e

        read -p "This will take the first SSD it can find and wipe it. Do you want to continue? (y/n) " answer
        if [ "$answer" == "y" ]; then
          # bootstrap disko an format the disks
          # Rather than cloning just grab the config
          curl "https://gitlab.com/slenderq/justin-nix/-/raw/main/systems/disko/lvm.nix" -o /tmp/disko-config.nix
          sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko /tmp/disko-config.nix
        fi


        read -p "Install NixOS? (y/n) " answer
        if [ "$answer" == "y" ]; then
          read -p "Enter the hostname for the system to install: " hostname

          sudo nixos-generate-config --no-filesystem --root /mnt

          # Install the flake based on the hostname
          nixos-install -v --flake "gitlab:slenderq/justin-nix#$hostname"

          # git clone https://gitlab.com/slenderq/justin-nix.git -o /mnt/home/justin/
        fi

      '';
    })
  ];
  system.stateVersion = "23.11";
}
