# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    # /etc/nixos/hardware-configuration.nix
    ./hardware/nixbase-hw.nix
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixbase"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  # networking.networkmanager.enable = true;

  # Enable network manager applet
  programs.nm-applet.enable = true;

  # Set your time zone.
  time.timeZone = "America/Edmonton";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_CA.UTF-8";

  # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # Enable the LXQT Desktop Environment.
  # services.xserver.displayManager.lightdm.enable = true;
  # services.xserver.desktopManager.lxqt.enable = true;

  # Configure keymap in X11
  # services.xserver = {
  #   layout = "us";
  #   xkbVariant = "";
  # };

  #  enable = true;
  # alsa.enable = true;
  # alsa.support32Bit = true;
  # pulse.enable = true;
  # # If you want to use JACK applications, uncomment this
  #jack.enable = true;

  # use the example session manager (no others are packaged yet so this is enabled by default,
  # no need to redefine it in your config for now)
  #media-session.enable = true;
  # };

  # Enable touchpad support (enabled default in most desktopManager).

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.justin = {
    isNormalUser = true;
    description = "justin";
    extraGroups = ["networkmanager" "wheel"];
    packages = with pkgs; [
      firefox
      #  thunderbird
    ];
  };

  home-manager.users.justin = {
    programs.kitty = {
      # browse themes
      # kitty +kitten themes
      #theme = "Japanesque";
      theme = "Glacier";
      # theme = "Fun Forrest";
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #  wget
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

  # need to make sure /export is owned by nobody:nogroup
  #  https://nixos.wiki/wiki/NFS

  fileSystems."/mnt/backup" = {
    device = "/dev/disk/by-uuid/b271e213-1bc9-4206-bbe1-5458899c50f4";
    fsType = "ext4";
  };
  fileSystems."/export/backup" = {
    device = "/mnt/backup";
    options = ["bind"];
  };

  fileSystems."/mnt/media" = {
    device = "/dev/disk/by-uuid/60a669fa-36a1-4dfa-bfd1-4f23bf74a4c0";
    fsType = "ext4";
  };
  fileSystems."/export/media" = {
    device = "/mnt/media";
    options = ["bind"];
  };

  fileSystems."/mnt/personal" = {
    device = "/dev/disk/by-uuid/f6ffaa20-049c-4fc7-b6ae-ee2d59a16a5d";
    fsType = "ext4";
  };
  fileSystems."/export/personal" = {
    device = "/mnt/personal";
    options = ["bind"];
  };
  services.nfs.server = {
    enable = true;
    # fixed rpc.statd port; for firewall
    lockdPort = 4001;
    mountdPort = 4002;
    statdPort = 4000;
    extraNfsdConfig = '''';
  };
  networking.firewall = {
    enable = true;
    # for NFSv3; view with `rpcinfo -p`
    allowedTCPPorts = [111 2049 4000 4001 4002 20048 5357 32400 3005 8324 32469 9090 445 139];
    allowedUDPPorts = [111 2049 4000 4001 4002 20048 3702 32410 32412 32413 32314 9090 137 138];
  };

  services.nfs.server.exports = ''
    /export         *(rw,fsid=0,sync)
    /export/backup  *(rw,nohide,insecure,sync,no_root_squash)
    /export/media  *(rw,nohide,insecure,sync,no_root_squash)
    /export/personal  *(rw,nohide,insecure,sync,no_root_squash)
  '';

  # workgroup = HOME
  services.samba-wsdd.enable = true; # make shares visible for windows 10 clients
  services.samba = {
    enable = true;
    settings = {
      global = {
        "server string" = "smbnix";
        "netbios name" = "smbnix";
        "security" = "user";
        "use sendfile" = "yes";
        "max protocol" = "smb2";
        # note: localhost is the ipv6 localhost ::1
        "hosts allow" = "192.168.0. 192.168.2 192.168.1 127. localhost";
        "guest account" = "nobody";
        "map to guest" = "bad user";
      };
      public = {
        path = "/mnt/media";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "valid users" = "justin";
        writeable = "yes";
        # "force user" = "justin";
        # "force group" = "";
      };
      personal = {
        path = "/mnt/personal";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "valid users" = "justin";
        writeable = "yes";
        # "force user" = "justin";
        # "force group" = "";
      };
      backup = {
        path = "/mnt/backup";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "valid users" = "justin";
        writeable = "yes";
        # "force user" = "justin";
        # "force group" = "";
      };
    };
  };

  # services.borgbackup.repos = {
  # nixpad = {
  # authorizedKeys = [
  # "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHt39Xbg5hmcHU+KWqjKYEdX4/h9MDfJeUKUK6w2y+ay root@nixpad"
  # ];
  # path = "/mnt/backup/hosts/nixpad";
  # };
  # };
  #
  services.tailscale.permitCertUid = "239";

  # reverse proxy for plex server:
  services.caddy = {
    enable = true;
    virtualHosts."nixbase.hedgehog-augmented.ts.net".extraConfig = ''
      reverse_proxy localhost:32400 {
        header_up -Referer
        header_up -X-Forwarded-For
        header_up Origin "nixbase.hedgehog-augmented.ts.net" "10.54.0.88"
        header_up Host "nixbase.hedgehog-augmented.ts.net" "10.54.0.88"
        header_down Location "10.54.0.88" "nixbase.hedgehog-augmented.ts.net"
      }
    '';
  };
  users.users."justin".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAi0v4eHGCPZUAfPLWYOMVzRenZO3HeBnUrfpq1EYs32 justin@nixbase" # build key
    # note: ssh-copy-id will add user@your-machine after the public key
    # but we can remove the "@your-machine" part
  ];

  services.uptime-kuma.enable = true;

  home-manager.users.justin = {
    home.file.".background-image".source = ../nixos/gui/backgrounds/manuel-sardo-ocean-unsplash.jpg;
  };
}
