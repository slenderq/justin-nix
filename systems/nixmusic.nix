# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  ...
}: {
  imports = [
    ./hardware/nixmusic-hw.nix
  ];

  # LVM defined in:
  # ./disko/lvm.nix
  networking.hostName = "nixmusic"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  services.thermald = {
    enable = true;
  };
  services.tlp.enable = true;

  # Set your time zone.
  time.timeZone = "America/Edmonton";

  services.fstrim.enable = true;

  services.throttled.enable = true;

  # Select internationalisation properties.
  i18n.defaultLocale = "en_CA.UTF-8";

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "us";
    xkb.variant = "";
  };

  users.users.justin = {
    isNormalUser = true;
    description = "justin";
    extraGroups = ["networkmanager" "wheel"];
    packages = with pkgs; [
      firefox
      #  thunderbird
    ];
  };
  home-manager.users.justin = {
    home.file.".background-image".source = ../nixos/gui/backgrounds/panagiotis-falcos-keyboard-unsplash.jpg;
  };

  # Enable automatic login for the user.
  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "justin";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    #  wget
  ];

  system.stateVersion = "23.11"; # Did you read the comment?

  fileSystems."/mnt/nixbase" = {
    device = "nixbase:/";
    fsType = "nfs";
    options = ["defaults" "soft" "bg"];
  };

  services.rpcbind.enable = true; # needed for NFS

  services.nfs.server = {
    enable = true;
    # fixed rpc.statd port; for firewall
    lockdPort = 4001;
    mountdPort = 4002;
    statdPort = 4000;
    extraNfsdConfig = '''';
  };
  networking.firewall = {
    enable = true;
    # for NFSv3; view with `rpcinfo -p`
    allowedTCPPorts = [111 2049 4000 4001 4002 20048 5357 32400 3005 8324 32469 9090 445 139];
    allowedUDPPorts = [111 2049 4000 4001 4002 20048 3702 32410 32412 32413 32314 9090 137 138];
  };

  fileSystems."/export" = {
    device = "/mnt/nixmusic";
    options = ["bind"];
  };

  services.nfs.server.exports = ''
    /export         *(rw,fsid=0,sync,nohide,insecure,sync,no_root_squash)
  '';
}
