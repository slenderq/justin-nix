{
  lib,
  stdenv,
  ...
}: {
  myself = "justinquaintance";
  users = {
    justinquaintance = {
      name = "Justin Quaintance";
      email = "justin.quaint@protonmail.com";
      sshKeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKC/tXRrpgjYX1Be1l0l+m9EWZb+Qz5qEQeiMjEkYXjk justin@cmdzero.io"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGRQjfHqCDcZ4JxxetLqkhFGQembcElEnjThXH+uPUEB justin@cmdzero.io"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDQGAEic4hQoqmjwjdCIRyvxSLWxqSQ7bwDX72urlNREqgkpzjlai0eKCMcZHk1K92mCEy4Ntd8pThME2QvJs7D0udL7mCjaF7y+nHwIw4a7Ree9P+YYQlBhYw2djCzRbwhku4+f/TYhSjrSXZbUhglSOVZgdIqRORJ+w648eaH3aHh2aD+MZEYvxuJfC0YtjIypvkqcV5h1/5kt2dUcMvEvUjEW9CTP/+OPv0kO3vfRJuT6xt3c3jQQ2amCBGj9wxtE/TYgj/B7zwg54TcdLi4vOQozegs9322XZDyicCbME/7lnnsBVrQdm5tkNEtAIGl4A+pzkHy5rGcpAtG31r6n9rb9gcGJcAWdgvaBig3F1yT9WfyYGdoTRZeZECgh5T/hm2nYKCExQe4t3TgTVs9P4WdCkDKijYM5AVV+jbfnUIc4GUokt5qXJP+FUNTDGMzhIK4SAdOAjZmYhknrVTLUsyaH0aCZP+AIVYnY2NcI5R538w/qI7OWBOfMojDKB0= justinquaintance@Justins-MacBook-Pro.local"
      ];
    };
  };
}
