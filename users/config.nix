{
  lib,
  stdenv,
  ...
}: {
  myself = "justin";
  users = {
    justin = {
      name = "Justin Quaintance";
      email = "justin.quaint@protonmail.com";
      sshKeys = [
        # todo; seperate this data into its own file. then i can reference it with agebox
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC/4Wq6OwiaW0eWRsb1HUu/HCgeiLyIjVCOSE3V2646zs29Y9Oq+WjTbj7aP5O0cvCXCslw0FqI4tD9QKzEg22ZIvuK7Y7IOyhXPq8p2Qq8ZLrOJ6ChNDDrxy4QvdurCpEioj8vvmAMxL1LfzaruLK0oFv2W0+hro69tH/vZ/vSm6cvxqe64rDuHWcK/lZ7Qux6RzKj9J59e4OEo5vkVMAglkFvGhAEDa6f0OZiOFn2XYB0Fqoi58mIQ27b9OfcJV/PHqZq9aVdIBU6Tn8qpbOJ/LhemB0vaRHG5brv9GUJZHnDIqhxLRBTgz+lJglBa1tNZiV04mlt7taT5CR+IqB6UiHs5Fp4GbHYKyO4UwqmPcCwnrKK95t3dudFYqmlSab1vRIX1NC6LbqdG7GmTg9l1jkOrPPMR/y+DTco/hBZWaMLnaWGB6vPsl2z2V/8qfoIfJijOSo10Ou9s4o0ZL8nHm/KyY8hX526fS3IhCAkXMaG/kJKTRMriuSUK1fY2WE= justin@manjaropad"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIILTV/AFIseKgHy57bulQwoHZm45kvkSo+yeMjLg4m29 justin@nixpad"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwdbRNy/bsnX37pJ302tl7vqCn1cSvrD3SS49tOBosqAqVmNxTnRDI3QlYnnNnKKzcDQqpAPiRr48PVT/GyLc6S04TKEarLhGOjNef+pUEr5wGlv94G6x8cOmYJcWE57nMZ1HoaynFL8h+WCatqeRv61UPuYsV7M+Q9b83Qyb61jjl+6x3baTnlP305up86DzqBI5GQmS4uK8UAgfZlc0LAnasN8aH+Vt2jR9FP8AofS1h8A277Z2JOV+GB55fDTKpE1X5Cd+xeRgNbzIBdhb59GqjFirjgQlrFiy+pLjg5XdkIXwEg/+I5Qd4MvmtAXd6lESSoGzfdFnNbJ9ODyvdzUFxScbiLrLwC9SveqKDvR8NDILF1yvvuu42TULIpbuhUo0+LGhwWrEAbvL3E7VqL0lzM/0zV+0dG8RkliG0ZvedC+oMihWDYb1dsKSEhAsx7R0e6KVCsK+I3mdO9k2QD8hPSnIXSa90P0ybBbwFudKm8u2OzBO2w8UmPRqJlaM= justin@vm-base"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCLtmwmO7/vtqnu2erHWv0qwfb844N6vaOnIKDvxV0YTzMPG+BF9oJjaiA5EYYvcfmoeciwxbkPJZSvwRC52v/hzibVgA2dl557QOq7weDtXfKw/69GnoZugZBg4Vm5b+Vf16+OJn561d4zTRjjhGOrLy+hkEn5NTneI4qzh/LZsQ/AWAiGzLswpkqrnvzt7nDBSlKKPikWPNeVH3B93b6YH/xqiyBgR6GvpH6TOElqBWXj8J22M1Gavlpq0CVgdMN7uur343T7XdnJYCbt3bg9de6LaUciJu7kof2UV09yv9A9vNKJPbJl0yoSGQLvb45qUm4nkdJC4JtpE/gUgLIOk3N7EbjeGn43R8tm1ZT4ojUb5Pr2rO6qcyqf24Q770EFBfKeoyZjRaLhsJZC+PT1/qiZo+sWGE2vnm4C3Lg9zScN/ay3bQQGMKLTMFEOEITzVvjQk8q8K02jI2Km1s8EbcDsJq3i9prP7lBB33kmf58EXntTxT4a0GRgI+yifoU= justin@nixbase"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFD1iR77tgeXrDtU6ngP07oYDztLIDDDc55zQSZTu1cmLxoze6XIRBeEvjQT9pT3ZxMAeKs+AEL7seQiuWAbciIaITYuNcxp4DbPVbZHDhkZ6Bu3ay5nMHVwRncVjkczSGRjr4Di4ko6VdJoZdemp6KYthirV9iTzwMZ9FpxDIwZoG5dl8pYiL23o1fSmyVLEvfHhDPtP9lX3nBW9Kkp3pCgq9MwecG9V+KnCEOSxWfZddzYN0jWCIVTNPZwjdogLkeO3NsjU04vINK+OYxU6u1DCi0myUxfzgP5lXJcKP7CQmanO/KhcRLUBhTbaDbe6/U0wROE76Zz7csxiOIunWBpelKCvh1lqC0RvCUpQcFBFcFX8PjYNzO16vTtnAVZ1xXhxX/096kRnCahSxD9XE4EwTr6Uf73Y9nXmWEM2KqhezZO/oHEUHMji3znHQKxDcJRKDtFxO22H01ho5zO5c77a3Qwl2vtZri0/r156YTguEbofs3gc9z3vY24YfWu0= justin@debianbook"
      ];
    };
  };
}
