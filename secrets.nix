# https://github.com/ryantm/agenix
let
  nixpad = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIILTV/AFIseKgHy57bulQwoHZm45kvkSo+yeMjLg4m29 justin@nixpad";

  vm-base = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCwdbRNy/bsnX37pJ302tl7vqCn1cSvrD3SS49tOBosqAqVmNxTnRDI3QlYnnNnKKzcDQqpAPiRr48PVT/GyLc6S04TKEarLhGOjNef+pUEr5wGlv94G6x8cOmYJcWE57nMZ1HoaynFL8h+WCatqeRv61UPuYsV7M+Q9b83Qyb61jjl+6x3baTnlP305up86DzqBI5GQmS4uK8UAgfZlc0LAnasN8aH+Vt2jR9FP8AofS1h8A277Z2JOV+GB55fDTKpE1X5Cd+xeRgNbzIBdhb59GqjFirjgQlrFiy+pLjg5XdkIXwEg/+I5Qd4MvmtAXd6lESSoGzfdFnNbJ9ODyvdzUFxScbiLrLwC9SveqKDvR8NDILF1yvvuu42TULIpbuhUo0+LGhwWrEAbvL3E7VqL0lzM/0zV+0dG8RkliG0ZvedC+oMihWDYb1dsKSEhAsx7R0e6KVCsK+I3mdO9k2QD8hPSnIXSa90P0ybBbwFudKm8u2OzBO2w8UmPRqJlaM= justin@vm-base";
  nixbase = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCLtmwmO7/vtqnu2erHWv0qwfb844N6vaOnIKDvxV0YTzMPG+BF9oJjaiA5EYYvcfmoeciwxbkPJZSvwRC52v/hzibVgA2dl557QOq7weDtXfKw/69GnoZugZBg4Vm5b+Vf16+OJn561d4zTRjjhGOrLy+hkEn5NTneI4qzh/LZsQ/AWAiGzLswpkqrnvzt7nDBSlKKPikWPNeVH3B93b6YH/xqiyBgR6GvpH6TOElqBWXj8J22M1Gavlpq0CVgdMN7uur343T7XdnJYCbt3bg9de6LaUciJu7kof2UV09yv9A9vNKJPbJl0yoSGQLvb45qUm4nkdJC4JtpE/gUgLIOk3N7EbjeGn43R8tm1ZT4ojUb5Pr2rO6qcyqf24Q770EFBfKeoyZjRaLhsJZC+PT1/qiZo+sWGE2vnm4C3Lg9zScN/ay3bQQGMKLTMFEOEITzVvjQk8q8K02jI2Km1s8EbcDsJq3i9prP7lBB33kmf58EXntTxT4a0GRgI+yifoU= justin@nixbase";
  nixgaming = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFD1iR77tgeXrDtU6ngP07oYDztLIDDDc55zQSZTu1cmLxoze6XIRBeEvjQT9pT3ZxMAeKs+AEL7seQiuWAbciIaITYuNcxp4DbPVbZHDhkZ6Bu3ay5nMHVwRncVjkczSGRjr4Di4ko6VdJoZdemp6KYthirV9iTzwMZ9FpxDIwZoG5dl8pYiL23o1fSmyVLEvfHhDPtP9lX3nBW9Kkp3pCgq9MwecG9V+KnCEOSxWfZddzYN0jWCIVTNPZwjdogLkeO3NsjU04vINK+OYxU6u1DCi0myUxfzgP5lXJcKP7CQmanO/KhcRLUBhTbaDbe6/U0wROE76Zz7csxiOIunWBpelKCvh1lqC0RvCUpQcFBFcFX8PjYNzO16vTtnAVZ1xXhxX/096kRnCahSxD9XE4EwTr6Uf73Y9nXmWEM2KqhezZO/oHEUHMji3znHQKxDcJRKDtFxO22H01ho5zO5c77a3Qwl2vtZri0/r156YTguEbofs3gc9z3vY24YfWu0= justin@debianbook";
  nixgaming_host = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIM1CxKxiO/Jm3OR9fJvKdWFyYZDB2WtvM/kWKst+ptJY root@nixos";
  nixpad_host = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKTGdoW1Xlma62fLpdd2TE/8QdFvAdRF/j0lYxtZ+bKn host key";

  nixbase_host = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICtJd09EqXEUvgJpBGw4AaVgVPbPb33SLUbRM1RzSahu root@nixbase";
  nixmusic = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN4szQh1jn8fUFKpCpxZS4sNItg72H2hxFr2SoU7+u02 justin@nixmusic";
  nixmusic_host = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFmgs9ce0362OVoI1j/18qDsi/jyWb/RcibPilFZr8Vv host key";
  nixbook = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIP8P0ZRNTOyCKrjGLTiYffVLvdXVfx4mHLItYKcL1Cbr justin@nixos";
  nixbook_host = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGvxXePY4ewIZ1HZj0zgK6Ki7qdf6VYLF2uVn90ZDlzG host key";

  nixexit = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGYK9CkHUADDjcRuh9Q6mYXn23zyW5HoUmv7wMhpvHln justin@nixexit";
  nixexit_host = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB6VYWWrAYLi1pnOGmWQ52dEtm/mbotvzQfYk4smMMx8 host key";

  bootstrap = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFycwXBZCQk+xTA4T692LlgWqFiuFMXT/2hmt/66WOo9 justin@nixvirt";

  # Locked out? use the host key.

  # allows us to boot with working wifi as we have decrypt using the key that is generated by this nixconfig
  # the ssh keys are needed for bootstrapping
  # host = builtins.readFile "/etc/ssh/ssh_host_ed25519_key.pub";
  all_keys = [nixbase nixbase_host vm-base nixgaming nixgaming_host nixpad nixpad_host nixmusic nixmusic_host nixbook nixbook_host nixexit_host nixexit];
in {
  "nixos/secrets/wireless-networks.env.age".publicKeys = all_keys ++ [bootstrap];
  "nixos/secrets/telegram.secret.age".publicKeys = all_keys;
  # Only use host keys, i.e. you need to building from the host in order to get the build key
  # i.e. allowing a whitelist so every host that gets secrets doesn't get a buildkey
  "nixos/secrets/buildkey.age".publicKeys = all_keys;
  "nixos/secrets/repopass.age".publicKeys = all_keys;
  "nixos/secrets/borgkey.age".publicKeys = all_keys;
  "nixos/secrets/ducksecret.age".publicKeys = all_keys;
  "nixos/secrets/pcsx2.age".publicKeys = all_keys;
  "nixos/secrets/openai.age".publicKeys = all_keys;
  "nixos/secrets/anthropic.age".publicKeys = all_keys;
  "nixos/secrets/tailscale.age".publicKeys = all_keys ++ [bootstrap];
  "nixos/secrets/gitlab.age".publicKeys = all_keys ++ [bootstrap];
  "nixos/secrets/secretNixConf.age".publicKeys = all_keys ++ [bootstrap];
  # "nixos/secrets/github.age".publicKeys = [ nixpad_host ]; # for now only use this on nixpad
}
