{
  pkgs,
  lib,
  config,
  ...
}: let
  inherit (lib) mkIf;
in {
  home.packages = [pkgs.grc];
  programs.fish = {
    enable = true;
    shellAbbrs = rec {
      nd = "nix develop -c $SHELL";
    };
    shellAliases = {
      # Clear screen and scrollback
      clear = "printf '\\033[2J\\033[3J\\033[1;1H'";
    };
    plugins = [
      {
        name = "grc";
        src = pkgs.fishPlugins.grc.src;
      }
      {
        name = "done";
        src = pkgs.fishPlugins.done.src;
      }
      {
        name = "colored-man-pages";
        src = pkgs.fishPlugins.colored-man-pages.src;
      }
      {
        name = "sponge";
        src = pkgs.fishPlugins.sponge.src;
      }
      {
        name = "bobthefisher";
        src = pkgs.fishPlugins.bobthefisher.src;
      }
      {
        name = "puffer-fish";
        src = pkgs.fishPlugins.puffer.src;
      }
      {
        name = "fzf";
        src = pkgs.fishPlugins.fzf.src;
      }
      {
        name = "foreign-env";
        src = pkgs.fishPlugins.foreign-env.src;
      }
    ];
    functions = {
      # Disable greeting
      fish_greeting = "";
      # Grep using ripgrep and pass to nvim
      # Merge history upon doing up-or-search
      # This lets multiple fish instances share history
      up-or-search =
        /*
        fish
        */
        ''
          if commandline --search-mode
            commandline -f history-search-backward
            return
          end
          if commandline --paging-mode
            commandline -f up-line
            return
          end
          set -l lineno (commandline -L)
          switch $lineno
            case 1
              commandline -f history-search-backward
              history merge
            case '*'
              commandline -f up-line
          end
        '';
    };
    interactiveShellInit =
      /*
      fish
      */
      ''

        if [ -f "$HOME/bin/private.fish" ]
             source  $HOME/bin/private.fish
        end

        # set SHELL (which fish)

        # Use whatever vim is around
        set -x EDITOR nvim
        set -x GIT_EDITOR nvim
        set -x SUDO_EDITOR nvim

        # FIXME: Actually grab the open ai secret from here.
        # source ~/bin/justin-shell/secret.fish

        set -x TERM  xterm-256color
        zoxide init fish --cmd cd | source
        # starship init fish | source
        gh completion -s fish | source

        # Generate a unique name using the date and time
        set unique_name (date "+%Y%m%d%H%M")
        # Check if the TMUX variable is not already set
        if not set -q TMUX
            # Set the TMUX variable to start a new Tmux session with the unique name without attaching to it
            set -g TMUX tmux new-session -d -s $unique_name -c fish

            # FIXME: when I install tmux
            # Start the Tmux session
            eval $TMUX

            # Attach to the Tmux session with the unique name without detaching other clients
            tmux attach-session -d -t $unique_name
        end


        # start message
        fortune tao | cowsay -f eyes

        if [ -f "/run/agenix/gitlab" ]
            glab auth login --stdin < /run/agenix/gitlab > /dev/null 2>&1
        end

        if [ -f "/run/agenix/github" ]
            gh auth login --with-token < /run/agenix/github> /dev/null 2>&1
        end

      '';
  };
}
