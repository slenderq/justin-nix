{
  config,
  pkgs,
  lib,
  comfortable-motion,
  numbers,
  vim-scimark,
  telescope-emoji-nvim,
  emojinvim,
  gruvbox,
  cellular-automaton,
  ...
}: {
  programs.neovim = {
    plugins = with pkgs.vimPlugins; [
      # Visual
      {
        plugin = pkgs.vimUtils.buildVimPlugin {
          name = "comfortable-motion";
          src = comfortable-motion;
        };
      }
      {
        # use insert mode to get real line numbers
        plugin = pkgs.vimUtils.buildVimPlugin {
          name = "myusuf3/numbers.vim";
          src = numbers;
        };
        type = "vim";
        config = ''
          let g:numbers_exclude = ['tagbar', 'gundo', 'minibufexpl', 'nerdtree']
        '';
      }
      {
        # Spreadsheets
        plugin = pkgs.vimUtils.buildVimPlugin {
          name = "vim-scimark";
          src = vim-scimark;
        };
      }
      {
        plugin = pkgs.vimUtils.buildVimPlugin {
          name = "cellular-automaton-nvim";
          src = cellular-automaton;
          type = "lua";
          confg = ''
            # note these don't seem to be actually added to the config
            # HACK: just added to the config file
            vim.keymap.set("n", "<leader>jj", "<cmd>CellularAutomaton make_it_rain<CR>")
            vim.keymap.set("n", "<leader>jl", "<cmd>CellularAutomaton game_of_life<CR>")
          '';
        };
      }
      {
        plugin = pkgs.vimUtils.buildVimPlugin {
          name = "telescope-emoji-nvim";
          src = telescope-emoji-nvim;
          type = "lua";
          confg = ''
            require("telescope").load_extension("emoji")
            vim.keymap.set("n", "<leader>jq", "<cmd>Telescope emoji search<CR>")
            require("telescope").setup {
              extensions = {
                emoji = {
                  action = function(emoji)
                    -- argument emoji is a table.
                    -- {name="", value="", cagegory="", description=""}

                    vim.fn.setreg("*", emoji.value)
                    print([[Press p or "*p to paste this emoji]] .. emoji.value)

                    -- insert emoji when picked
                    -- vim.api.nvim_put({ emoji.value }, 'c', false, true)
                  end,
                }
              },
            }
          '';
        };
      }
      {
        plugin = pkgs.vimUtils.buildVimPlugin {
          name = "emoji.nvim";
          src = emojinvim;
          type = "lua";
          confg = ''
            # note these don't seem to be actually added to the config
            vim.keymap.set("n", "<leader>fe", "<cmd>:Telescope emoji<CR>")
          '';
        };
      }
      {
        plugin = pkgs.vimUtils.buildVimPlugin {
          name = "gruvbox.nvim";
          src = gruvbox;
          type = "lua";
          confg = ''
            require("gruvbox").setup()
          '';
        };
      }
      # eandrju/cellular-automaton.nvim " particle simulation.
      # :command Mir CellularAutomaton make_it_rain
      # (fromGitHub "HEAD" "elihunter173/dirbuf.nvim")
    ];
  };
}
