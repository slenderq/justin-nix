{pkgs, ...}: {
  programs.neovim = {
    plugins = with pkgs.vimPlugins; [
      {
        # https://github.com/nvim-telescope/telescope-ui-select.nvim
        plugin = telescope-ui-select-nvim;
        type = "lua";
        config = ''
          -- This is your opts table
          require("telescope").setup {
            extensions = {
              ["ui-select"] = {
                require("telescope.themes").get_dropdown {
                  -- even more opts
                }

                -- pseudo code / specification for writing custom displays, like the one
                -- for "codeactions"
                -- specific_opts = {
                --   [kind] = {
                --     make_indexed = function(items) -> indexed_items, width,
                --     make_displayer = function(widths) -> displayer
                --     make_display = function(displayer) -> function(e)
                --     make_ordinal = function(e) -> string
                --   },
                --   -- for example to disable the custom builtin "codeactions" display
                --      do the following
                --   codeactions = false,
                -- }
              }
            }
          }
          -- To get ui-select loaded and working with telescope, you need to call
          -- load_extension, somewhere after setup function:
          require("telescope").load_extension("ui-select")
        '';
      }
      {
        plugin = telescope-nvim;
        type = "lua";
        config = ''
          nmap("<leader>ff", ":Telescope find_files<cr>")
          nmap("<leader>fg", ":Telescope live_grep<cr>")
          nmap("<leader>fb", ":Telescope buffers<cr>")
          nmap("<leader>fh", ":Telescope help_tags<cr>")
          nmap("<leader>ft", ":Telescope git_files<cr>")
          nmap("<leader>fo", ":Telescope oldfiles <cr>")
          nmap("<leader>fz", ":Telescope current_buffer_fuzzy_find <cr>")
          nmap("<leader>fr", ":Telescope resume <cr>")
        '';
      }
      {
        plugin = telescope-zoxide;
        type = "lua";
        config = ''
          nmap("<leader>fz", ":Telescope zoxide list<cr>")
        '';
      }
      {
        plugin = telescope-file-browser-nvim;
        type = "lua";
        config = ''
          -- You don't need to set any of these options.
          -- IMPORTANT!: this is only a showcase of how you can set default options!
          require("telescope").setup {
            extensions = {
              file_browser = {
                theme = "ivy",
                mappings = {
                  ["i"] = {
                    -- your custom insert mode mappings
                  },
                  ["n"] = {
                    -- your custom normal mode mappings
                  },
                },
              },
            },
          }
          -- To get telescope-file-browser loaded and working with telescope,
          -- you need to call load_extension, somewhere after setup function:
          require("telescope").load_extension "file_browser"
          nmap("<leader>fb", ":Telescope file_browser path=%:p:h<cr>")
          nmap("<leader>fB", ":Telescope file_browser<cr>")
        '';
      }
    ];
  };
}
