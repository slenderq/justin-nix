{pkgs, ...}: {
  # https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions
  programs.neovim = {
    # lang server
    # C-y, C-n, C-p
    coc = {
      enable = true;
      settings = {
        "languageserver" = {
          "nix" = {
            command = "nil";
            filetypes = ["nix"];
            rootPatterns = ["flake.nix"];
            # Uncomment these to tweak settings.
            # settings = {
            #   nil = {
            #     formatting = { command = ["nixfmt"]; };
            #   };
            # };
          };

          terraform = {
            command = "terraform-ls";
            args = ["serve"];
            filetypes = ["terraform" "tf"];
            initializationOptions = {};
            settings = {};
          };
        };
      };
    };
    extraPackages = [
      pkgs.nodejs # coc requires nodejs
      # https://github.com/oxalica/nil?tab=readme-ov-file
      pkgs.nil
      pkgs.terraform-ls
      pkgs.coc-pyright
    ];
  };
}
