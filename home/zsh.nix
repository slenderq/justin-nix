{...}: {
  programs.zsh.enable = true;
  programs.zsh.envExtra = ''
  '';

  programs.nix-index.enableZshIntegration = true;
}
