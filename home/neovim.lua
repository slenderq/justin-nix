-- -------
-- Library
-- -------

function map (mode, shortcut, command)
vim.api.nvim_set_keymap(mode, shortcut, command, { noremap = true, silent = true })
end
function nmap(shortcut, command)
map('n', shortcut, command)
end
function imap(shortcut, command)
map('i', shortcut, command)

end

-- ------
-- Config
-- ------
--
-- Make sure there are no space 
vim.cmd([[
let mapleader = ";"

set runtimepath^=~/.vim runtimepath+=~/.vim/after
set clipboard+=unnamedplus
:imap jk <Esc> " TODO: replace with plugin as it is slow.

syntax on

:setlocal spell

set nobackup " no swp files
" set termguicolors " 24-bit colors

" Highlight my yank
" https://jdhao.github.io/2020/05/22/highlight_yank_region_nvim/
augroup highlight_yank
    autocmd!
    au TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=700}
augroup END

let g:numbers_exclude = ['tagbar', 'gundo', 'minibufexpl', 'nerdtree']

colorscheme gruvbox

]])

-- I don't care about tabs.
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.smartindent = true

-- Or folds
vim.opt.foldlevel = 20

vim.keymap.set("n", "<leader>jj", "<cmd>CellularAutomaton make_it_rain<CR>")
vim.keymap.set("n", "<leader>jl", "<cmd>CellularAutomaton game_of_life<CR>")

vim.keymap.set("n", "<leader>jd", "<cmd>r! date<CR>I# ")

-- https://neovim.io/doc/user/lua.html#vim.filetype.add%28%29
vim.filetype.add({
  extension = {
   tsx = 'typescriptreact',
  },
})


