{
  pkgs,
  lib,
  ...
}:
# Platform-independent terminal setup
{
  home.packages = with pkgs; [
    # Setting the correct locale for the shell is necessary. The required locale is glibcLocales.
    # glibcLocales

    coreutils # A collection of basic Unix tools for file, shell, and text manipulation
    sudo
    which # A utility to show the full path of commands
    xdg-utils # A set of command-line utilities for interacting with the desktop environment
    xterm # for good mesure
    less

    # Version control
    gh # Github cli
    tig # Tig is a command-line utility for viewing repositories.

    # Terminal customizations
    colordiff # A tool to colorize diff output
    diffutils
    starship # A customizable prompt for shells

    # Neovim

    # File management
    tree # A utility to display directory structures in a tree-like format
    rsync # sync and better cp operations

    # Search and find
    fd # A simple, fast and user-friendly alternative to find
    fastmod # A tool to modify files using search and replace operations
    # Example:
    #    https://github.com/facebookincubator/fastmod
    #    fastmod -m -d /home/jrosenstein/www --extensions php,html \
    #    '<font *color="?(.*?)"?>(.*?)</font>' \
    #    '<span style="color: ${1};">${2}</span>'
    ripgrep # A line-oriented search tool that combines the usability of The Silver Searcher with the raw speed of grep
    sd # A tool to find and replace text in files
    tokei # A program that displays statistics about your code

    # Productivity
    googler # A command-line utility for Google web and site searching
    tldr # A collection of simplified and community-driven man pages
    cheat # A collection of examples for various command-line tools
    jq # A lightweight and flexible command-line JSON processor
    mdp # A command-line based markdown presentation tool
    tmux # A terminal multiplexer
    shellcheck # A static analysis tool for shell scripts
    lazygit # A simple terminal UI for Git commands
    gitui # A blazing fast terminal UI for Git written in Rust
    dmenu # A dynamic menu for X11
    man-db # man pages
    man-pages
    bc # terminal calculator
    gnused
    gnugrep
    gawk
    nb # KISS command line notes tool.
    sc-im # spreadsheets with vi keybindings

    # Document viewing
    mdcat # A command-line tool for rendering markdown files in the terminal
    # TODO: set this up with HN
    newsboat # A console RSS reader
    glow # A nice markdown viewer for the terminal
    w3m # web browser
    links2

    # Performance testing
    hyperfine # A command-line benchmarking tool

    # Disk usage
    du-dust # A more intuitive version of the du command

    # Networking
    nmap
    ntp
    openresolv

    # System monitoring
    glances # Top replacement
    htop # another top replacement
    btop # has a futuristic sci-fi appearance.

    # https://github.com/charmbracelet/vhs
    vhs # record and playback term stuff
    asciinema # similar but seen in the wild?
    age # encryption tool
    agebox # manage age encrypted files in a repo

    # https://github.com/adrianlopezroche/fdupes
    fdupes

    # Shell
    zsh
    nushell

    eza

    # Useful for Nix development
    nixci
    nil # Nix language Server
    nixpkgs-fmt

    # Work
    grafana-loki

    # Linux Crisis Tools
    procps # ps(1), vmstat(8), uptime(1), top(1) - basic stats
    util-linux # dmesg(1), lsblk(1), lscpu(1) - system log, device info
    sysstat # iostat(1), mpstat(1), pidstat(1), sar(1) - device stats
    iproute2 # ip(8), ss(8), nstat(8), tc(8) - preferred net tools
    numactl # numastat(8) - NUMA stats
    tcpdump # tcpdump(8) - Network sniffer
    bpftrace # bpftrace, basic versions of opensnoop(8), execsnoop(8), runqlat(8), biosnoop(8), etc. - eBPF scripting[1]
    trace-cmd # trace-cmd(1) - Ftrace CLI
    ethtool # ethtool(8) - net device info
    tiptop # tiptop(1) - PMU/PMC top
    msr-tools # rdmsr(8), wrmsr(8) - CPU digging

    hyfetch # neofetch with pride flags <3
    unp
    unrar
    p7zip
    traceroute
    archivemount

    mermaid-cli # text file based graph language
    kitty-themes
    _1password-cli
    _1password-gui
    cowsay
    fortune
    glab
    ncdu # disk space search
    pipes
    figlet
    nix-search-cli # nix-search command
    atop # alternative top
    iftop # networking monitor that gives histograms
    iotop # io monitoring with lots of detail
    nvtopPackages.full # network card top
    wavemon # monitor wifi
    ddgr
    yazi
  ];

  home.shellAliases = rec {
    # Shorten things
    ll = "ls -alF";
    la = "ls -A";
    l = "ls -CF";
    c = "clear";

    # If your terminal supports colors, use them!
    ls = "ls --color=auto";
    grep = "grep --color=auto";
    fgrep = "fgrep --color=auto";
    egrep = "egrep --color=auto";
    diff = "colordiff";

    # git alias
    ga = "git add -u -p";
    gs = "git status --short";
    grb = "GIT_EDITOR=nvim git rebase -i";
    gc = "GIT_EDITOR=nvim git commit";
    gpo = "git push --set-upstream origin \"$(git rev-parse --abbrev-ref HEAD)\"";
    gl = "git log --pretty=fuller";
    gch = "git checkout";
    gb = "git branch --sort=-committerdate";

    edit-unmerged = "vim `git diff --name-only --diff-filter=UU`";
    add-unmerged = "git add `git diff --name-only --diff-filter=UU`";
    #   TODO: actually get glow to work with nvim this is fine for now
    # alias glow='EDITOR=vim glow'

    rg = "rg --color=always";

    bc = "bc -l";

    dc = "docker compose";
    beep = "say 'beep'";
    tf = "terraform";

    # neofetch was always queer?
    # it always has been 🔫
    neofetch = "hyfetch";
  };

  xdg = {
    enable = true;
    # rm /home/justin/.config/hyfetch.json
    configFile = {
      "hyfetch" = {
        source = ./hyfetch.json;
        target = "hyfetch.json";
      };
    };
  };

  programs = {
    bash.enable = true;
    bat.enable = true;
    autojump.enable = false;
    zoxide.enable = true;
    fzf.enable = true;
    jq.enable = true;
    ranger = {
      enable = true;
    };
    # nix-index.enable = true;
    # htop.enable = true;
    # rio.enable = true;
  };
}
