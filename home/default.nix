{self, ...}: {
  flake = {
    homeModules = {
      common = {
        home.stateVersion = "24.11";
        imports = [
        ];
      };
      common-linux = {
        imports = [
          self.homeModules.common
          #           ./linux-terminal.nix
        ];
        # targets.genericLinux.enable = true;
      };
      common-darwin = {
        imports = [
          self.homeModules.common
          ./bash.nix
          # ./kitty.nix
          # ./emacs.nix
        ];
      };
      hyprland = {
        imports = [
          self.homeModules.common
          ./kitty.nix
          ./hyprland
          # ./wayland-wm
        ];
      };
    };
  };
}
