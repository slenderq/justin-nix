{
  pkgs,
  lib,
  ...
}: {
  # logs: ~/.cache/starship/session_${STARSHIP_SESSION_KEY}.log
  #  󱙷 󱐨  󱃅 󱦤 󰲓 󰨡 󱗃 󰻖  󰹧 󱃏  󰧑 󱝶  󰦱 󱐟  󰭆 󰳨 󰷚 󱇪 󱐚 󱜿 󱎶 󱥰                   󰑜  󰚀  󰐂  󰐼  󰠪  󰟟    󰮭  󰯉  󰮯  󰻳 󱁂  󱁕  󱡜 󱢇    󱩏              󰣚
  # https://www.messletters.com/en/emoticons/
  # idea, different icons per shell
  # 󰚀  󰐂  󰐼  󰠪  󰟟  󰡷 󱋾
  # 󰤨 󰮭  󰯉  󰮯  󰻳 󱁂 󱁏 󱁊 󱁕
  # 󱁢  󱡜 󱢇  󱢆 󱢺 󱩏      󰜗
  # 󰢼  󰢽  󰢾          󰴽
  # 󰜗
  # 
  # 󱡔
  # 󰣚 󰦠 󱛠
  #  󰅏  󰞦 󰾧
  programs.starship = {
    enable = true;
    settings = builtins.fromTOML ''
      # Get editor completions based on the config schema
      "$schema" = 'https://starship.rs/config-schema.json'

      # add_newline = true

      # https://emojicombos.com/horizontal-line-symbols

      # https://www.nerdfonts.com/cheat-sheet

      #╭──────╮
      #│ test │
      #╰──────╯
      format = """
         [$fill](#5b5b5b)


      ╭─━ $username$hostname in $directory󰔌󰔌󰔌󰔌\
      $git_status$git_branch$git_commit󰔌󰔌󰔌󰔌$nix_shell󰔌󰔌󰔌󰔌$aws$fill ''${custom.version}\
      ─━╮
      $character
      """
      # Right Prompt
      right_format = """
      $cmd_duration $os$sudo$shell$python$terraform$docker$golang $battery─━╯
      """

      [username]
      style_user = 'bold green'
      #style_root = 'black bold'
      format = '[$user]($style) '
      disabled = false
      show_always = true

      [cmd_duration]
      format = "  [$duration]($style) ─━"
      style = "default"
      show_milliseconds = true
      min_time = 2

      [time]
      disabled = false
      format = '󰥔 [ $time ]($style) '
      # time_format = '%T'
      use_12hr = true
      utc_time_offset = '-5'
      # time_range = '10:00:00-14:00:00'


      # FIXME $hostname not working T

      #$fill $cmd_duration
      # palette = "Belafone"

      [fill]
      symbol = '󰔌'
      # symbol = '╾─'
      style = 'default'

      [directory]
      read_only = " 󰌾"
      read_only_style = 'red'
      use_os_path_sep=true
      truncation_length=3
      style = "green"


      [env_var.USER]
      format = '[$env_value]($style)'
      variable = 'USER'
      default = '󰟼'
      # style = 'bold fg:green'

      # TODO: consider using status
      [character]
      # https://www.messletters.com/en/emoticons/
      success_symbol = "╰━─<◕ᴗ◕>"
      error_symbol = "╰━─<x.x>"
      #success_symbol = "╰━─━<•ᴗ•>(purple)"
      # error_symbol = "╰━─━≥༎ຶД༎ຶ.≤(purple)"
      # error_symbol = "╰━─━<༎ຶД༎ຶ>(purple)"
      # error_symbol = "╰━─━•O•(purple)"
      # success_symbol = "╰━─━ 󰺕 x 󰺕  (purple)"
      # success_symbol = "╰━─━ 󰴈 u 󰴈 (purple)"
      # success_symbol = "╰━─━ 󰮕 .󰮕 (purple)"
      #success_symbol = "╰━─━      (purple)"
      # success_symbol = "╰━─━le>(purple)"
      # error_symbol = "╰── <()>(red)"
      vimcmd_symbol = "╰──➤ [❮](green)"

      [shell]
      # style="yellow"
      disabled = false
      bash_indicator = '[󱆃](#003366) bash'
      zsh_indicator = '[](#9c52f2) zsh'
      fish_indicator = '[󰈺](#8fce00) fish'
      nu_indicator = '[❱](#b4dbff) nu'
      powershell_indicator = ''
      unknown_indicator = '?'

      [sudo]
      style = 'green'
      symbol = ' '
      format = '$symbol($style)'
      disabled = false

      [terraform]
      symbol='󱁢 '
      format = '$symbol($style)'

      # 󱄅

      # https://starship.rs/presets/nerd-font.html
      # style = "yellow"

      [python]
      symbol = " "
      format = "[$symbol](#ffe100)"
      style = "yellow"

      # https://www.friedrichkurz.me/blog/2021/11/06/starship-custom-prompt-components/

      [custom.version]
      command = 'cat ~/bin/version'
      # command = 'cat $HOME/bin/version'
      format = '[$symbol($output) ]($style)'
      when = "true"

      [custom.symbolgen]
      command = 'python3 $HOME/bin/python/symbolgen.py' # shows output of command
      format = '[$symbol($output)]($styl)'
      when = "true"

      [custom.docker]
      description = "Shows the docker symbol if the current directory has Dockerfile or docker-compose.yml files"
      command = "echo  "
      files = ["Dockerfile", "docker-compose.yml", "docker-compose.yaml"]
      when = """ command -v docker &> /dev/null; exit (echo $?); """

      #  merging
      # 

      [git_branch]
      symbol = ' '
      # '󰴽 '
      format = ' [$symbol$branch(:$remote_branch)]($style) '
      style = "#9c52f2"

      [git_commit]
      format = '[$hash$tag]($style) '
      only_detached = false

      [git_state]
      merge = '(ﾉ˚Д˚)ﾉ '

      [git_status]
      stashed = ""
      format = ' $all_status$ahead_behind '
      # (ﾉ˚Д˚)ﾉ
      up_to_date = "[](green)"
      # up_to_date = "[ (๑˃̵ᴗ˂̵)و ](green)"
      modified = '[](red)'
      # modified = '[ (ﾉ˚Д˚)ﾉ ](red)'
      ahead = '⇡''${count}'
      diverged = '⇕⇡''${ahead_count}⇣''${behind_count}'
      behind = '⇣''${count}'

      [aws]
      symbol = "  "
      format = " [$symbol($profile )($region)($duration] )]($style)"

      [battery]
      full_symbol = '󱈏 '
      charging_symbol = '󰂄 '
      format = ' [$symbol$percentage]($style) '

      [[battery.display]]
      threshold = 90
      discharging_symbol = '󰂂 '
      style = 'green'

      [[battery.display]]
      threshold = 80
      discharging_symbol = '󰂁 '


      [[battery.display]]
      threshold = 70
      discharging_symbol = '󰂀 '


      [[battery.display]]
      threshold = 60
      discharging_symbol = '󰁿 '


      [[battery.display]]
      threshold = 50
      discharging_symbol = '󰁾 '

      [[battery.display]]
      threshold = 40
      discharging_symbol = '󰁽 '
      style = 'yellow'

      [[battery.display]]
      threshold = 30
      discharging_symbol = '󰁼 '

      [[battery.display]]
      threshold = 20
      discharging_symbol = '󰁻 '
      style = 'red'

      [[battery.display]]
      threshold = 10
      discharging_symbol = '󰁺 '
      style = 'red'


      [[battery.display]] # 'bold yellow' style and 💦 symbol when capacity is between 10% and 30%
      threshold = 30
      style = 'bold yellow'
      discharging_symbol = '💦'

      [golang]
      symbol = " "
      format = "[$symbol($version )]($style)"
      style = "blue"

      [nix_shell]
      symbol = " "
      format = " [$symbol $name]($style) $state "
      impure_msg = '[impure](yellow)'
      pure_msg = '[pure](green)'
      style = "#3D7AB8"
      # heuristic = true

      [hostname]
      ssh_only = false
      format = '[$ssh_symbol](bold blue)on [$hostname](bold red)'
      trim_at = '.companyname.com'
      disabled = false


      # ------------------------------------------
      [buf]
      symbol = " "

      [c]
      symbol = " "

      [conda]
      symbol = " "

      [dart]
      symbol = " "


      [docker_context]
      symbol = " "

      [elixir]
      symbol = " "

      [elm]
      symbol = " "

      [fossil_branch]
      symbol = " "



      [guix_shell]
      symbol = " "

      [haskell]
      symbol = " "

      [haxe]
      symbol = "⌘ "

      [hg_branch]
      symbol = " "


      [java]
      symbol = " "

      [julia]
      symbol = " "

      [lua]
      symbol = " "

      [memory_usage]
      symbol = "󰍛 "

      [meson]
      symbol = "󰔷 "

      [nim]
      symbol = "󰆥 "


      [nodejs]
      symbol = " "

      [os]
      disabled = false

      [os.symbols]
      Alpaquita = " "
      Alpine = " "
      Amazon = " "
      Android = " "
      Arch = " "
      Artix = " "
      CentOS = " "
      Debian = " "
      DragonFly = " "
      Emscripten = " "
      EndeavourOS = " "
      Fedora = " "
      FreeBSD = " "
      Garuda = "󰛓 "
      Gentoo = " "
      HardenedBSD = "󰞌 "
      Illumos = "󰈸 "
      Linux = " "
      Mabox = " "
      Macos = " "
      Manjaro = " "
      Mariner = " "
      MidnightBSD = " "
      Mint = " "
      NetBSD = " "
      NixOS = " "
      OpenBSD = "󰈺 "
      openSUSE = " "
      OracleLinux = "󰌷 "
      Pop = " "
      Raspbian = " "
      Redhat = " "
      RedHatEnterprise = " "
      Redox = "󰀘 "
      Solus = "󰠳 "
      SUSE = " "
      Ubuntu = " "
      Unknown = " "
      Windows = "󰍲 "

      [package]
      symbol = "󰏗 "

      [pijul_channel]
      symbol = "🪺 "


      [rlang]
      symbol = "󰟔 "

      [ruby]
      symbol = " "

      [rust]
      symbol = " "

      [scala]
      symbol = " "

      [spack]
      symbol = "🅢 "



      [palettes.Belafone]
      background   = "#20111a"
      foreground   = "#958b83"
      cursor       = "#958b83"
      selection_back = "#45363b"
      color0       = "#20111a"
      color8       = "#5e5252"
      color1       = "#bd100d"
      red       = "#bd100d"
      color9       = "#bd100d"
      color2       = "#858062"
      green       = "#858062"
      color10      = "#858062"

      color3       = "#e9a448"
      yellow      = "#e9a448"
      # yellow      = "#e9a448"
      color4       = "#416978"
      samon = "#d89d72"
      pink = "#d8727b"
      violet ="#9c52f2"
      color12      = "#416978"
      color5       = "#96522b"
      color13      =        "#96522b"
      color6       =        "#98999c"
      color14      =        "#98999c"
      color7       =        "#958b83"
      white=        "#d4ccb9"
      s
    '';
  };
}
