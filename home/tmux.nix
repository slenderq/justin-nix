{
  pkgs,
  config,
  ...
}: {
  programs.tmux = {
    enable = true;
    plugins = with pkgs; [
      tmuxPlugins.better-mouse-mode
      tmuxPlugins.vim-tmux-navigator
      tmuxPlugins.gruvbox
      tmuxPlugins.yank
      tmuxPlugins.continuum
      tmuxPlugins.resurrect
      tmuxPlugins.better-mouse-mode
      tmuxPlugins.tmux-fzf
      tmuxPlugins.fzf-tmux-url
      tmuxPlugins.fingers
    ];
    # potential
    # https://github.com/yardnsm/tmux-1password
    extraConfig = ''

      set-option -g prefix C-s
      unbind-key C-b
      bind-key C-a send-prefix

      # Send prefix in nested session
      bind-key -n C-g send-prefix

      set -g base-index 1
      set -g set-clipboard on
      # Create sensible default tmux keybindings for me coming from a pure i3 vim workflow
      # Easy config reload
      bind-key R source-file ~/.config/tmux/tmux.conf \; display-message "tmux.conf reloaded."

      bind-key Z resize-pane -Z

      # Set default-terminal to kitty
      set -g default-terminal "tmux-256color"

      set -g base-index 1
      setw -g aggressive-resize on

      set-option -g default-shell $SHELL
      # set-option -g default-shell /usr/bin/fish

      set -g @tmux-gruvbox 'dark' # or 'light'

      set-option -g history-limit 100000000
      set-option -g renumber-windows on
      set-option -g status-interval 1
      set-option -wg word-separators ' '
      set-option -g mouse on

      bind-key : command-prompt
      bind-key enter next-layout
      bind-key C-o rotate-window
      bind-key c new-window
      bind-key C-c new-window
      bind-key C copy-mode
      bind-key P paste-buffer
      bind-key -n MouseUp2Pane paste

      # navigation
      bind-key v split-window -h -c "#{pane_current_path}"
      bind-key C-v split-window -h -c "#{pane_current_path}"
      bind-key s split-window -v -c "#{pane_current_path}"
      bind-key C-s split-window -v -c "#{pane_current_path}"
      bind-key h select-pane -L
      bind-key j select-pane -D
      bind-key k select-pane -U
      bind-key l select-pane -R
      bind-key C-n next-window
      bind-key C-p previous-window
      bind-key S set-window-option synchronize-panes
      bind C-l send-keys 'C-l'
      bind C-k send-keys 'C-k'
      bind-key C-Left swap-window -t -1
      bind-key C-Right swap-window -t +1

      # Smart pane switching with awareness of Vim splits.
      # See: https://github.com/christoomey/vim-tmux-navigator
      is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
          | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
      bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
      bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
      bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
      bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
      bind-key -T copy-mode-vi C-h select-pane -L
      bind-key -T copy-mode-vi C-j select-pane -D
      bind-key -T copy-mode-vi C-k select-pane -U
      bind-key -T copy-mode-vi C-l select-pane -R

      # copying
      bind -T copy-mode-vi v send-keys -X begin-selection
      bind -T copy-mode-vi C-v send-keys -X rectangle-toggle
      bind -T copy-mode-vi y \
          send-keys -X copy-selection \; \
          send-keys -X clear-selection

      bind -T copy-mode-vi MouseDown3Pane \
          send-keys -X copy-selection-and-cancel

      unbind -n MouseDragEnd1Pane
      unbind -T copy-mode-vi MouseDragEnd1Pane
      bind -T copy-mode-vi MouseDrag1Pane \
          send-keys -X clear-selection \; \
          send-keys -X begin-selection
      bind -n MouseDrag1Pane \
          copy-mode -M \; \
          send-keys -X clear-selection \; \
          send-keys -X begin-selection

      # Double LMB Select & Copy (Word)
      bind-key -n DoubleClick1Pane \
          select-pane \; \
          copy-mode -M \; \
          send-keys -X select-word
      bind-key -T copy-mode-vi DoubleClick1Pane \
          select-pane \; \
          send-keys -X select-word

      # Triple LMB Select & Copy (Line)
      bind-key -T copy-mode-vi TripleClick1Pane \
          select-pane \; \
          send-keys -X select-line
      bind-key -n TripleClick1Pane \
          select-pane \; \
          copy-mode -M \; \
          send-keys -X select-line

      setw -g mode-keys vi

      # colors
      set-option -g status-fg black
      set-option -g status-bg default

      # Set window notifications
      setw -g monitor-activity on
      set -g visual-activity on

      # battery
      # not working
      # https://github.com/tmux-plugins/tmux-battery
      # set -g status-right '#{battery_status_bg} Batt: #{battery_icon} #{battery_percentage} #{battery_remain} | %a %h-%d %H:%M '
      # tmuxPlugins.battery


    '';
  };
}
