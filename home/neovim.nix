{pkgs, ...}: {
  imports = [
  ];
  programs.neovim = {
    enable = true;

    extraPackages = with pkgs; [
      python311Packages.pynvim # Python bindings required for certain plugins in neovim.
      tree-sitter
    ];

    # Full list here,
    # https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/editors/vim/plugins/generated.nix
    plugins = with pkgs.vimPlugins; [
      # https://github.com/itchyny/lightline.vim
      lightline-vim
      vim-css-color
      # kvim-devicons
      nvim-web-devicons

      ## Sidebar
      vim-peekaboo # JIT register preview
      vim-signature # Display marks on the side.

      # Extra features
      {
        plugin = vim-sneak; # s [two letters] , f style search
        config = "let g:sneak#label = 1";
      }
      vim-surround # ys[char] ds[char] rs[char]
      vim-abolish # autocorrect on common mistakes
      {
        plugin = nvim-lastplace;
        type = "lua";
        config = ''
          require("nvim-lastplace").setup();
        '';
      }

      {
        plugin = vim-suda;
        config = "let g:suda_smart_edit = 1";
      } # sudo in vim

      ## Git
      {
        plugin = vim-fugitive;
        type = "lua";
        config = ''
          nmap("<leader>gs", ":Git status<cr>")
          nmap("<leader>ga", ":Git add -u -p<cr>")
          nmap("<leader>gc", ":Git commit<cr>")
          nmap("<leader>gp", ":Git push<cr>")
          nmap("<leader>gA", ":Git add %<cr>")
          nmap("<leader>gC", ":Git commit --amend<cr>")
        '';
      }
      {
        plugin = blamer-nvim; # inline git blame
        config = ''
          let g:blamer_enabled = 0
          let g:blamer_date_format = '%Y-%m-%dT%H:%M:%S'
        '';
      }

      ## Tmux
      tmux-nvim
      vim-tmux-navigator

      # Util
      terminus # For working mouse support when running inside tmux

      # File management
      {
        plugin = nerdtree;
        config = ''
          let NERDTreeShowHidden=1
          :command Nt NERDTreeToggle
          :nnoremap <C-p> :NERDTreeToggle<CR>
        '';
      }
      {
        plugin = oil-nvim;
        type = "lua";
        config = ''
          require("oil").setup()
        '';
      }
      zoxide-vim # better cd

      # Language support
      # https://neovim.io/doc/user/treesitter.html
      # https://github.com/nvim-treesitter/nvim-treesitter#troubleshooting
      ale
      vim-nix
      vim-terraform
      vim-go
      # vim-illuminate
      vim-markdown
      {
        plugin = nvim-treesitter;
        type = "lua";
        # because we are on nix, install parsers here
        config = ''
          require('nvim-treesitter.configs').setup({
            ensure_installed = { "markdown", "nix", "terraform", "python", "bash" , "lua", "vim", "vimdoc", "query" },
            parser_install_dir = "~/.config/nvim",
            indent = {
              enable = true
            },
            textobjects = {
              select = {
                enable = true,

                -- Automatically jump forward to textobj, similar to targets.vim
                lookahead = true,

                keymaps = {
                  -- You can use the capture groups defined in textobjects.scm
                  ["af"] = "@function.outer",
                  ["if"] = "@function.inner",
                  ["ac"] = "@class.outer",
                  -- You can optionally set descriptions to the mappings (used in the desc parameter of
                  -- nvim_buf_set_keymap) which plugins like which-key display
                  ["ic"] = { query = "@class.inner", desc = "Select inner part of a class region" },
                  -- You can also use captures from other query groups like `locals.scm`
                  ["as"] = { query = "@scope", query_group = "locals", desc = "Select language scope" },
                },
                -- You can choose the select mode (default is charwise 'v')
                --
                -- Can also be a function which gets passed a table with the keys
                -- * query_string: eg '@function.inner'
                -- * method: eg 'v' or 'o'
                -- and should return the mode ('v', 'V', or '<c-v>') or a table
                -- mapping query_strings to modes.
                selection_modes = {
                  ['@parameter.outer'] = 'v', -- charwise
                  ['@function.outer'] = 'V', -- linewise
                  ['@class.outer'] = '<c-v>', -- blockwise
                },
                -- If you set this to `true` (default is `false`) then any textobject is
                -- extended to include preceding or succeeding whitespace. Succeeding
                -- whitespace has priority in order to act similarly to eg the built-in
                -- `ap`.
                --
                -- Can also be a function which gets passed a table with the keys
                -- * query_string: eg '@function.inner'
                -- * selection_mode: eg 'v'
                -- and should return true of false
                include_surrounding_whitespace = true,
              },
            },
            highlight = {
              enable = true,

              -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
              -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
              -- the name of the parser)
              -- list of language that will be disabled
              disable = { "c", "rust" },
              -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
              disable = function(lang, buf)
                  local max_filesize = 100 * 1024 -- 100 KB
                  local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
                  if ok and stats and stats.size > max_filesize then
                      return true
                  end
              end,

              -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
              -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
              -- Using this option may slow down your editor, and you may see some duplicate highlights.
              -- Instead of true it can also be a list of languages
              additional_vim_regex_highlighting = true,
            },
          })
        '';
      }
    ];

    # Add library code here for use in the Lua config from the
    # plugins list above.
    extraLuaConfig = ''
      ${builtins.readFile ./neovim.lua}
    '';
  };
}
