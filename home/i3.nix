{
  config,
  lib,
  pkgs,
  ...
}: {
  xdg = {
    enable = true;
    configFile = {
      "i3status" = {
        source = ./i3status;
        target = "i3status";
        recursive = true;
      };
      "i3" = {
        source = ./i3;
        target = "i3";
        recursive = true;
      };
    };
  };
}
