{
  pkgs,
  lib,
  ...
}:
# Platform-independent terminal setup
{
  home.packages = with pkgs; [
    # Setting the correct locale for the shell is necessary. The required locale is glibcLocales.
    # glibcLocales
    iproute2
    zathura # A document viewer with a minimalistic and space-saving interface
  ];
  # https://discourse.nixos.org/t/nixpkgs-nixos-unstable-many-package-fail-with-glibc-2-38-not-found/35078/3
  home.sessionVariables = {
    # TODO: Not sure what the right way of doing this is...
    # The list of packages is a subset of `GI_TYPELIB_PATH` in `nix-shell -p gobject-introspection gtk4`.
    GI_TYPELIB_PATH = "$GI_TYPELIB_PATH:${pkgs.gobject-introspection.out}/lib/girepository-1.0:${pkgs.gtk4.out}/lib/girepository-1.0:${pkgs.graphene.out}/lib/girepository-1.0:${pkgs.gdk-pixbuf.out}/lib/girepository-1.0:${pkgs.harfbuzz.out}/lib/girepository-1.0:${pkgs.pango.out}/lib/girepository-1.0";
    LD_LIBRARY_PATH = "$LD_LIBRARY_PATH:${pkgs.glib.out}/lib";
  };
}
