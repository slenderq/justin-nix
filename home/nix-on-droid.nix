# Minimal Phone configuration
{pkgs, ...}: let
  # Fully persisted and backed up links
  mkPersistentLink = path:
    pkgs.runCommand "persistent-link" {} ''
      ln -s /nix/persistent/justin/${path} $out
    '';
in {
  environment.packages = with pkgs; [
    openssh
    clang

    (writeShellApplication {
      name = ",nix-on-droid-build";
      text = ''
        #!/usr/bin/env bash
        cd ~/justin-nix
        nix-on-droid switch --flake .#default --show-trace
      '';
    })
  ];

  imports = [
  ];

  environment.motd = ''
      _______________________________
    < Welcome to Nix on Droid Phone! >
      -------------------------------
             \   ^__^
              \  (oo)\_______
                 (__)\       )\/\
                     ||----w |
                     ||     ||
  '';

  home-manager.config = {
    # Read home-manager changelog before changing this value
    home.stateVersion = "23.11";

    imports = [
      ./git.nix
      ./terminal.nix
      ./neovim.nix
      ./neovim/telescope.nix
      ./neovim/coc.nix
      ./fish.nix
      ./tmux.nix
    ];

    # allow unfree, etc.
    nixpkgs.config = import ./nixpkgs-config.nix;
  };
  system.stateVersion = "23.11";

  terminal.colors = {
    background = "#1d2021";
    foreground = "#ebdbb2";
    color0 = "#282828";
    color8 = "#928374";
    color1 = "#cc241d";
    color9 = "#fb4934";
    color2 = "#98971a";
    color10 = "#b8bb26";
    color3 = "#d79921";
    color11 = "fabd2f";
    color4 = "#458588";
    color12 = "83a598";
    color5 = "#b16286";
    color13 = "d3869b";
    color6 = "#689d6a";
    color14 = "8ec07c";
    color7 = "#a89984";
    color15 = "ebdbb2";
  };

  terminal.font = ./termux/font.ttf;
}
