{
  pkgs,
  config,
  ...
}: {
  home.packages = [pkgs.git-lfs pkgs.difftastic];

  programs.git = {
    package = pkgs.gitAndTools.gitFull;
    enable = true;
    # HACK: hardcoding this until I figure out how to get flakes working on nix on droid
    userName = "Justin Quaintance";
    userEmail = "justinquaintancec@gmail.com";
    ignores = ["*~" "*.swp"];
    delta = {
      enable = true;
      options = {
        features = "decorations";
        navigate = true;
        light = false;
        side-by-side = true;
      };
    };
    extraConfig = {
      core.editor = "nvim";
      credential.helper = "store --file ~/.git-credentials";
      pull.rebase = "false";
      diff.external = "difft";
      fetch.recurseSubmodules = "true";
      init.defaultBranch = "main";
    };
  };

  programs.lazygit = {
    enable = true;
    settings = {
      # This looks better with the kitty theme.
      gui.theme = {
        lightTheme = false;
        activeBorderColor = ["white" "bold"];
        inactiveBorderColor = ["white"];
        selectedLineBgColor = ["reverse" "white"];
      };
    };
  };
}
