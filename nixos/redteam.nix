{
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    # wifi
    wifite2
    aircrack-ng
    kismet
  ];
  services.printing.enable = true;
}
