{...}: {
  fileSystems."/home/justin/.cache" = {
    device = "none";
    fsType = "tmpfs";
    options = ["defaults" "size=25%" "mode=755" "uid=justin"];
  };

  environment.sessionVariables = {
    XDG_CACHE_HOME = "$HOME/.cache";
  };
}
