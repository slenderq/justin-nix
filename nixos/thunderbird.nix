{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}: {
  home-manager.users."justin" = {
    programs.thunderbird.profiles = {
      justin = {
        isDefault = true;
      };
    };
    home.packages = with pkgs; [
      protonmail-bridge
      thunderbird
    ];
    xsession = {
      enable = true; # this feels cracked why do I have to do this?
      windowManager.i3 = {
        enable = true;
      };
    };
  };

  services.gnome.gnome-keyring.enable = true;

  systemd.user.services.protonmailbridge = {
    enable = true;
    serviceConfig = {
      # protonmail-bridge --cli
      ExecStart = "protonmail-bridge -n";
      Restart = "on-failure";
    };
    wantedBy = ["multi-user.target"]; # starts after login
  };
}
