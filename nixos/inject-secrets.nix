{
  config,
  lib,
  pkgs,
  ...
}: {
  # https://github.com/ryantm/agenix
  age.secrets.wireless-networks = {
    file = ./secrets/wireless-networks.env.age;
    owner = "justin";
    group = "users";
  };

  age.secrets.telegram-secret = {
    file = ./secrets/telegram.secret.age;
    path = "/etc/telegram.secret";
    owner = "justin";
    group = "users";
  };

  age.secrets.buildkey = {
    file = ./secrets/buildkey.age;
    path = "/etc/ssh/nixbase_id_ecdsa";
    mode = "600"; # less secret in a real system
    owner = "justin";
    group = "users";
  };

  age.secrets.borgkey = {
    file = ./secrets/borgkey.age;
    path = "/etc/ssh/ssh_borg_eded25519_key_agenix";
    mode = "600"; # less secret in a real system
    owner = "justin";
    group = "users";
  };

  age.secrets.ducksecret = {
    file = ./secrets/ducksecret.age;
    path = "/home/justin/.config/duckstation/bios/scph1001.bin";
    mode = "770"; # less secret in a real system
    owner = "justin";
    group = "users";
    symlink = false;
  };
  age.secrets.pcsx2 = {
    file = ./secrets/pcsx2.age;
    path = "/home/justin/.config/PCSX2/bios/scph39001.bin";
    mode = "770"; # less secret in a real system
    owner = "justin";
    group = "users";
    symlink = false;
  };
  age.secrets.openai = {
    file = ./secrets/openai.age;
    mode = "770"; # less secret in a real system
    owner = "justin";
    group = "users";
    path = "/etc/openai";
  };

  age.secrets.anthropic = {
    file = ./secrets/anthropic.age;
    mode = "770"; # less secret in a real system
    owner = "justin";
    group = "users";
  };

  age.secrets.repopass = {
    file = ./secrets/repopass.age;
    mode = "700"; # less secret in a real system
    owner = "justin";
    group = "users";
  };

  age.secrets.tailscale = {
    file = ./secrets/tailscale.age;
    owner = "justin";
    group = "users";
  };

  age.secrets.gitlab = {
    file = ./secrets/gitlab.age;
    owner = "justin";
    group = "users";
  };

  age.secrets.secretNixConf = {
    path = "/home/justin/.bbNix.conf";
    file = ./secrets/secretNixConf.age;
    owner = "justin";
    group = "users";
  };

  nix.extraOptions = "!include /home/justin/.bbNix.conf";

  age.identityPaths = [
    "/home/justin/.ssh/id_rsa"
    "/home/justin/.ssh/id_ed25519"
    "/home/justin/justin-nix/secrets/bootstrap"
    "/etc/ssh/ssh_host_ed25519_key"
  ];

  # IDEA: use this key for many things
  # - Use this for cluster secrets that need to be passed machine to machine.
  # - New users of the cluster submit PRs with their root key being added to the secrets

  # https://mynixos.com/nixpkgs/option/services.openssh.hostKeys
  # Host key to add to secrets.nix for a new machine.
  # Root owned key
  services.openssh.hostKeys = [
    {
      comment = "host key";
      path = "/etc/ssh/ssh_host_ed25519_key";
      type = "ed25519";
    }
  ];
}
