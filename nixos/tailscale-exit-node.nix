{
  config,
  pkgs,
  ...
}: {
  # This file assumes you have nixos/tailscale.nix imported into your config.

  # This file also assumes that you have enabled the hostname in the mullvad exit nodes in the console
  systemd.services.tailscale-exitnode = {
    enable = true;
    description = "Automatic connection to Tailscale";

    # make sure tailscale is running before trying to connect to tailscale
    after = ["network-pre.target" "tailscale-autoconnect.service"];
    wants = ["network-pre.target" "tailscale-autoconnect.service"];
    wantedBy = ["multi-user.target"];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    # sudo tailscale up --exit-node=ca-yyc-wg-201.mullvad.ts.net  --exit-node-allow-lan-access=true --accept-routes=true
    # have the job run this shell script
    script = with pkgs; ''
      # wait for tailscaled to settle
      sleep 2

      # otherwise authenticate with tailscale
      ${tailscale}/bin/tailscale up --exit-node=ca-yyc-wg-201.mullvad.ts.net  --exit-node-allow-lan-access=true --accept-routes=true --reset
    '';
  };
}
