{
  config,
  pkgs,
  lib,
  ...
}: let
  imports = [
    ./backup-montior.nix
  ];
in
  lib.mkIf (config.networking.hostName != "nixmusic") {
    fileSystems."/mnt/nixmusic" = {
      device = "nixmusic:/";
      fsType = "nfs";
      options = ["defaults" "soft" "bg"];
    };

    services.rpcbind.enable = true; # needed for NFS

    # Make sure nfs share are working properly
    services.logind.extraConfig = ''
      KillUserProcess=Yes
      KillExcludeUsers=root
      RemoveIPC=yes
    '';
  }
