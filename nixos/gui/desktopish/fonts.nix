{
  config,
  lib,
  pkgs,
  ...
}: {
  fonts = {
    enableDefaultFonts = true;
  };
}
