{
  config,
  pkgs,
  callPackage,
  ...
}: {
  imports = [
    ../wifi_supplicant.nix
  ];

  services.xserver = {
    enable = true;
    desktopManager = {
      xterm.enable = false;
      xfce = {
        enable = true;
        noDesktop = false;
        enableXfwm = true;
      };
    };
    windowManager.i3.enable = true;
  };
  services.displayManager.defaultSession = "xfce";
}
