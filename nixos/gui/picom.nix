{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}: {
  # https://nix-community.github.io/home-manager/options.html#opt-services.picom.enable
  services.picom = {
    enable = true;
    settings = {
      blur = {
        method = "dual_kawase";
        size = 5;
        strength = 4;
      };
    };
    backend = "glx";
    fade = true;
    # blur = true;
    inactiveOpacity = 0.8;
    vSync = true;
    shadow = true;
    fadeDelta = 19;
    opacityRules = [
      "100:class_g = 'firefox' && focused"
      "100:class_g = 'firefox' && !focused"
    ];
  };
}
