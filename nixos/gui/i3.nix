{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}: {
  services.xserver = {
    desktopManager = {
      xterm.enable = false;
      xfce = {
        enable = true;
        noDesktop = true;
        enableXfwm = false;
      };
    };
  };

  services.displayManager.defaultSession = "xfce";
  programs.dconf.enable = true;

  imports = [
    # ./wireless.nix
    ../wifi_supplicant.nix
  ];

  home-manager.users."justin" = {
    xsession = {
      enable = true; # this feels cracked why do I have to do this?
      windowManager.i3 = {
        enable = true;
      };
    };
  };

  # still use keyring
  services.gnome.gnome-keyring.enable = true;

  networking.networkmanager.enable = true;

  # https://mynixos.com/search?q=i3
  services.xserver.windowManager.i3 = {
    enable = true;
    package = pkgs.i3;

    extraSessionCommands = ''
      eval $(gnome-keyring-daemon --daemonize)
      export SSH_AUTH_SOCK
    '';

    extraPackages = with pkgs; [
      # picom
      dmenu #application launcher most people use
      i3status # gives you the default i3 status bar
      i3lock #default i3 screen locker
      i3blocks #if you are planning on using i3blocks over i3status
      i3-swallow
      feh
      autorandr
      udiskie
      upower
      rofi
      nitrogen
      xfce.thunar
      xfce.thunar-volman
      xfce.tumbler
      pcmanfm
      rsync
      clipit
      google-chrome
      firefox
      xfontsel
      font-manager
      autorandr
      gtk4
      kitty
      i3lock
      xautolock
      scrot
      pamixer
      playerctl
      (writeShellApplication {
        name = ",enable-i3-debug";
        text = ''
          #!/usr/bin/env bash
          i3-msg 'debuglog on; shmlog on; reload'
        '';
      })
    ];
  };

  # this helps get i3blocks to get system info properly
  environment.pathsToLink = ["/libexec"];

  # TODO: Split these out into their own config.
  fonts.packages = with pkgs; [
    fira-code-nerdfont
    # nerdfonts.droid-sans-mono
    # nerdfonts.droid-sans-mono
    #  noto-fonts
    #  noto-fonts-cjk
    #  font-awesome
    #  noto-fonts-emoji
    #  liberation_ttf
    #  fira-code
    #  fira-code-symbols
    #  mplus-outline-fonts.githubRelease
    #  dina-font
    #  proggyfonts
  ];
  fonts.fontDir.enable = true;
  fonts.enableDefaultPackages = true;

  fonts.fontconfig = {
    antialias = true;
    cache32Bit = true;
    hinting.enable = true;
    hinting.autohint = true;
    defaultFonts = {
      monospace = ["FiraCode Nerd Font"];
      sansSerif = ["FiraCode Nerd Font"];
      serif = ["FiraCode Nerd Font"];
    };
  };

  # assuming you have the current location set up.
  services.redshift = {
    enable = true;
  };
  systemd.user.services.clitit = {
    description = "Clipit Daemon";
    serviceConfig.PassEnvironment = "DISPLAY";
    script = ''
      clipit
    '';
    wantedBy = ["multi-user.target"]; # starts after login
  };

  services.udisks2.enable = true;
  programs.light.enable = true;
  services.actkbd = {
    enable = true;
    bindings = [
      {
        keys = [224];
        events = ["key"];
        command = "/run/current-system/sw/bin/light -A 10";
      }
      {
        keys = [225];
        events = ["key"];
        command = "/run/current-system/sw/bin/light -U 10";
      }
    ];
  };
}
