{config, ...}: {
  networking.wireless.enable = true; # Enables wireless support via wpa_supplicant.
  networking.wireless.userControlled.enable = true;

  networking.wireless.secretsFile = config.age.secrets.wireless-networks.path;

  networking.wireless.networks = {
    x4l8fw_ip-stealer = {
      # SSID with no spaces or special charactersj
      psk = "ext:PSK_HOME";
    };
    "Little Phone that Could".psk = "ext:PSK_HOME"; # phone wifi for when I'm not at home.
    "SHAW-C17F".psk = "ext:PSK_MER";
    "SAHG5".psk = "ext:PSK_ALSKYLAR";
    "Al Gore's Bubble Bath".psk = "ext:PSK_KENT";
  };
}
