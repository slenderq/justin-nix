{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    gnupg
    (pkgs.writeShellApplication {
      name = ",encrypt-bootstrap";
      text = ''
        gpg --pinentry-mode loopback -c "$HOME/justin-nix/nixos/secrets/bootstrap"
      '';
    })
    (pkgs.writeShellApplication {
      name = ",decrypt-bootstrap";
      text = ''
        gpg --pinentry-mode loopback -d "$HOME/justin-nix/nixos/secrets/bootstrap.gpg" > "$HOME/justin-nix/nixos/secrets/bootstrap"
      '';
    })
  ];
}
