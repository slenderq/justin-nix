{pkgs, ...}: {
  virtualisation.docker = {
    enable = true;
    autoPrune.enable = true;
    enableOnBoot = true;
    daemon.settings = {
      "log-driver" = "local";
      "log-opts" = {
        "max-size" = "10m";
        "max-file" = "5";
      };
      # specific mount for docker data
      "data-root" = "/mnt/personal/system";
    };
  };

  environment.systemPackages = with pkgs; [
    docker-compose
  ];

  users.users."justin" = {
    extraGroups = ["docker"];
  };
}
