{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    ollama-telebot
  ];

  # ollama run zephyr "Why is the sky blue?"
  systemd.user.services.telebot = {
    enable = true;
    description = "Ollama Telebot Serivce";
    serviceConfig = {
      ExecStart = "ollama_telebot";
      Restart = "on-failure";
    };
    wantedBy = ["multi-user.target"]; # starts after login
  };
}
