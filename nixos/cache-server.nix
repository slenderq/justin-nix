{
  pkgs,
  lib,
  config,
  nix-serve-ng,
  ...
}: {
  imports = [
    nix-serve-ng.nixosModules.default
  ];

  # Cache server
  services.nix-serve = {
    enable = true;
    # todo: actually sign our caches when they start getting more "public"
    #secretKeyFile = config.sops.secrets."cache-server/private-key".path;
  };
  nix.settings.allowed-users = ["nix-serve"];
  nix.settings.trusted-users = ["nix-serve"];
}
