{
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    slack
    postman
    brave # contains all the work browser stuff
    pomodoro-gtk
    pomodoro
    gh # todo: make sure the secrets for this are stored
  ];

  age.secrets.github = {
    file = ./secrets/github.age;
    owner = "justin";
    group = "users";
  };

  home-manager.users.justin = {
    home.file.".background-image".source = ./gui/backgrounds/ishfaq-ahmed-macbook-unsplash.jpg;
    programs.git = {
      extraConfig = {
        core.sshCommand = "ssh -i /home/justin/.ssh/id_ed25519bb";
      };
      userName = lib.mkForce "Justin Quaintance";

      userEmail = lib.mkForce "justin@bank-brain.com";
    };
    home.sessionVariables = {
      GIT_SSH_COMMAND = "ssh -i /home/justin/.ssh/id_ed25519bb";
    };
  };

  networking.nameservers = ["100.100.100.100" "1.1.1.1"];

  environment.sessionVariables.DEFAULT_BROWSER = "${pkgs.brave}/bin/brave";
}
