{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}: {
  imports = [
    ./music_scripts.nix
  ];
  #musnix.overlays = [
  #  (
  #    final: prev: {
  #      prev.musnix.powerManagement.cpuFreqGovernor = "powersave";
  #    }
  #  )
  #];
  # http://www.tedfelix.com/linux/linux-midi.html
  # musnix = { # note this just for this computer

  # Find this value with `lspci | grep -i audio` (per the musnix readme).
  # PITFALL: This is the id of the built-in soundcard.
  #   When I start using the external one, change it.
  musnix.enable = true;
  # musnix.soundcardPciId = "02:00.0";
  musnix.kernel.realtime = true;

  #  };
  environment.sessionVariables = rec {
    XDG_CACHE_HOME = "$HOME/.cache";
    XDG_CONFIG_HOME = "$HOME/.config";
    XDG_DATA_HOME = "$HOME/.local/share";
    XDG_STATE_HOME = "$HOME/.local/state";
    # QT_QA_PLATFORM_PLUGIN_PATH="${qt5.qtbase.bin}/lib/qt-${qt5.qtbase.version}/plugins/platforms";
    # Not officially in the specification
    XDG_BIN_HOME = "$HOME/.local/bin";
    PATH = [
      "${XDG_BIN_HOME}"
    ];
  };

  # Other Nix configuration options...
  environment.systemPackages = with pkgs; [
    libjack2
    jack2
    qjackctl
    pavucontrol
    libjack2
    jack2
    # jack2Full
    # wine
    wineWowPackages.staging
    winetricks
    bespokesynth
    yabridge
    yabridgectl
    hydrogen
    # renoise
    carla
    calf
    zynaddsubfx
    reaper
    audacity
    a2jmidid
    open-music-kontrollers.patchmatrix
    # open-music-kontrollers.sherlock
    helm
    zynaddsubfx
    dexed
    odin2
    surge
    calf
    # ace-plugins.eq
    # soler-eq
    # lsp-plugins # useful but too many
    delayarchitect
    # chow-matrix
    # ace-plugins.delay
    dragonfly-reverb
    # stone-phaser
    # chow-phaser
    autotalent
    # guitarix
    # chow-centaur
    sfizz
    drumgizmo
    hydrogen
    geonkick
    japa
    magnetophonDSP.LazyLimiter
    glib
    sooperlooper
    raysession
    abcmidi
    seq66
    gsettings-qt
    gsettings-desktop-schemas
    kde-gtk-config
    qastools
    gsettings-qt
    paulstretch
    airwindows-lv2
    noise-repellent
    # distrho
    bchoppr
    # gxplugins-lv2
    talentedhack
    artyFX
    # ams-lv2 # broken
    speech-denoiser
    x42-gmsynth
    sorcer
    bschaffl
    x42-avldrums
    # surge-XT
    bslizr
    midi-trigger
    # magnetophonDSP.ConstantDetuneChorus
    vocproc
    LibreArp-lv2
    qmidiarp
    aj-snapshot

    rakarrack # Multi-effects processor emulating a guitar effects pedalboard
    qtractor # Audio/MIDI multi-track sequencer
    petrifoo # Sample Workstation
    giada # A free, minimal, hardcore audio -tool for DJs, live performers and electronic musicians
    ardour # Multi-track hard disk recording software
    muse # MIDI/Audio sequencer with recording and editing capabilities
    youtube-dl
  ];
  # todo; make a yabridge setup script here
  # yabridgectl add .wine/drive_c/Program\ Files/Common\ Files/VST3/
  boot.kernelModules = ["snd-seq" "snd-rawmidi"];

  users.extraUsers.justin.extraGroups = ["jackaudio" "audio"];

  users.users.justin.packages = with pkgs; [
    CHOWTapeModel # Physical modelling signal processing for analog tape recording. LV2, VST3, CLAP, and standalone
  ];

  # systemd.services.sooper-start = {
  #   description = "Automatic sooperlooper Start";

  #  wantedBy = [ "multi-user.target" ];

  # serviceConfig = {
  #  User = "justin";
  #  Group = "users";
  #  PassEnvironment = "DISPLAY";
  #  ExecStart = "${pkgs.sooperlooper}/bin/sooperlooper  --load-midi-binding=/home/justin/bin/nixos/music_assets/default_midi.slb";
  #  Type = "simple";
  #  };
  # };

  # generate a list of nixpkgs packages for the kx.studio packs full suite
  # i.e. Carla etc

  # systemd.user.services.aj2midid = {
  # enable = true;
  # description = "A2jmidid Service";
  # serviceConfig = {
  # ExecStart = "a2jmidid --export-hw";
  # Restart = "on-failure";
  # };
  # wantedBy = [ "multi-user.target" ]; # starts after login
  # };
}
