# https://lazamar.co.uk/nix-versions/
{
  flake,
  pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/8ad5e8132c5dcf977e308e7bf5517cc6cc0bf7d8.tar.gz"),
  ...
}: {
  programs.fish = {
    enable = true;
  };
}
