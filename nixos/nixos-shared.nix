{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}: {
  # This module is for general goodies

  #imports = [ "/home/justin/bin/nixos/nixos-shared.nix" ]; # line for main nixconfig
  imports = [
  ];

  programs.fish.enable = true;
  users.defaultUserShell = pkgs.fish;
  # input
  services.libinput.enable = true;
  # sound.enable = true;

  # https://nixos.wiki/wiki/JACK
  # Add a seperate sound module that has all the sound stuff

  # allow shebang to be run more naturally
  # https://github.com/mic92/envfs
  services.envfs.enable = true;
  programs.nix-ld.enable = true;

  # 192.168.0.185 nixbase
  # 192.168.0.89 nixgaming
  networking.extraHosts = ''
    192.168.1.215 vm-base
    192.168.0.111 tvpi
    192.168.1.127 Justins-MBP
  '';
  time.timeZone = "America/Edmonton";

  programs.zsh.enable = true;

  # i18n.supportedLocales = [ "en_US.UTF-8/UTF-8" "en_US/ISO-8859-1" ];
  # i18n.defaultLocale = "en_US.UTF-8/UTF-8";
  # i18n.extraLocaleSettings = {
  # LANG = "en_US.UTF-8/UTF-8";
  # LC_MESSAGES = "en_US.UTF-8/UTF-8";
  # LC_IDENTIFICATION = "en_US.UTF-8/UTF-8";
  # LC_ALL = "en_US.UTF-8";

  # };
  # environment.systemPackages = with pkgs; [
  # fishPlugins.done
  # fishPlugins.fzf-fish
  # fishPlugins.forgit
  # fishPlugins.hydro
  # fishPlugins.grc
  # grc
  # ];

  # programs.fish.enable = true;
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.justin = {
    extraGroups = ["networkmanager" "docker"];
  };

  # Enable the OpenSSH daemon.
  services.openssh = {
    enable = true;
  };

  # **** Desktop ****
  services.xserver = {
    enable = true;
    autorun = true;
    xkb.layout = "us";
    xkb.variant = "";
  };

  # this helps get i3blocks to get system info properly
  environment.pathsToLink = ["/libexec"];

  # **** Network Services ****

  services.xrdp.enable = true;
  services.xrdp.openFirewall = true;
  services.xrdp.defaultWindowManager = "i3";

  # https://discourse.nixos.org/t/slow-build-at-building-man-cache/52365/3
  documentation.man.generateCaches = false;

  environment.sessionVariables = {
    NIXPKGS_ALLOW_UNFREE = 1;
  };

  # **** pkgs ****
  environment.systemPackages = with pkgs; [
    blueman
    bluez

    coreutils
    dnsmasq
    findutils
    gcc
    gcolor3
    gimp
    glibc
    google-chrome
    # iputils

    man-db
    man-pages
    mlocate
    mobile-broadband-provider-info
    nitrogen
    patch
    pcmanfm
    python310
    # qflipper-git

    texinfo
    tk
    w3m
    udiskie
    upower
    usbutils
    util-linux
    # vibrancy-icons-teal
    # volumeicon
    w3m
    xdg-utils
    xdotool
    libinput
    xterm
    wget
    lxappearance
    iwd
    firefox
    kitty
    openssh
    w3m
    wirelesstools
    wavemon
    linssid

    # Document viewing
    zathura # A document viewer with a minimalistic and space-saving interface

    rxvt-unicode

    tailscale
    xorg.xbacklight

    pavucontrol

    libgdiplus
    remmina
    freerdp
    lxrandr
    arandr
    filezilla
    lftp
    bc
    btop
    gtk3
    gtkmm3
    pciutils
    autorandr
    # rclone
    pkg-config
    xarchiver
    nb
    gtk4
    gtk3

    gtk3-x11

    lxqt.libqtxdg # qt implementation of freedesktop.org xdg
    libqt5pas
    xdg-desktop-portal-gtk
    lxqt.xdg-desktop-portal-lxqt
    glib
    xfce.gigolo
    xfce.tumbler
    xfce.xfce4-terminal
    xfce.xfce4-taskmanager
    gruvbox-dark-icons-gtk
    numix-solarized-gtk-theme
    gsettings-desktop-schemas
    obs-studio
    cheese
    qbittorrent
    qutebrowser
    gparted
    raylib-games
    home-manager
    mullvad-vpn
    xmountains
    oneko # cute cat
    czkawka # gui to find dupes and much more
    xorg.ico
    xlife
    dig
  ];
}
