{
  self,
  pkgs,
  lib,
  nixpkgs,
  ...
}: {
  # make the nix shell point to a real tmp dir
  # systemd.services.nix-daemon.environment.TMPDIR = "/var/tmp/nix-daemon";

  nix = {
    package = pkgs.nixVersions.stable;
    # nixPath = ["nixpkgs=${nixpkgs}"]; # Enables use of `nix-shell -p ...` etc
    # registry.nixpkgs.flake = nixpkgs; # Make `nix shell` etc use pinned nixpkgs

    # https://nixos.wiki/wiki/Storage_optimization
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 30d";
    };

    settings = {
      max-jobs = "auto";
      connect-timeout = 1;
      log-lines = lib.mkDefault 25;
      max-free = lib.mkDefault (3000 * 1024 * 1024);
      min-free = lib.mkDefault (512 * 1024 * 1024);
      fsync-metadata = false;
      keep-build-log = false;
      preallocate-contents = true;

      experimental-features = "nix-command flakes auto-allocate-uids configurable-impure-env";
      # I don't have an Intel mac.
      extra-platforms = lib.mkIf pkgs.stdenv.isDarwin "aarch64-darwin x86_64-darwin";
      # Nullify the registry for purity.
      # flake-registry = builtins.toFile "empty-flake-registry.json" ''{"flakes":[],"version":2}'';
      trusted-users = ["root" "@wheel"];
      #allowed-uris = ["https://tarballs.nixos.org" "https://python.org" "https://developer.download.nvidia.com" "https://download.nvidia.com" "https://proxy.golang.org" "https://registry.npmjs.org"];
      trusted-public-keys = ["cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=" "cache.garnix.io:CTFPyKSLcx5RMJKfLo5EEPUObbA78b0YQ2DTCJXqr9g="];
      #extra-substituters = ["https://aseipp-nix-cache.global.ssl.fastly.net"];
    };
  };
}
