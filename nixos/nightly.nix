{
  pkgs,
  ...
}: {
  systemd.services.nightly-build = {
    enable = true;
    description = "Nightly Build Service";

    script = ''
      ${pkgs.nixci}/bin/nixci build "gitlab:slenderq/justin-nix"
    '';

    wantedBy = ["multi-user.target"]; # starts after login
  };

  systemd.timers.nightly-build = {
    wantedBy = ["timers.target"];
    timerConfig = {
      OnBootSec = "24h";
      OnUnitActiveSec = "24h";
      Unit = "nightly-build.service";
    };
  };
}
