# https://shb.skarabox.com/blocks-backup.html#blocks-backup-options
{
  pkgs,
  config,
  lib,
  ...
}: let
  repos = [
    {
      path = "/mnt/nixbase/backups";
      timerConfig = {
        OnCalendar = "00:00:00";
        RandomizedDelaySec = "3h";
      };
    }
    {
      path = "s3:s3.us-west-000.backblazeb2.com/backups";
      timerConfig = {
        OnCalendar = "16:00:00";
        RandomizedDelaySec = "3h";
      };
    }
  ];
in
  lib.mkIf (config.networking.hostName != "nixbase") {
    shb.backup.instances.home = {
      enable = true;

      backend = "restic";

      keySopsFile = ./secrets.yaml;

      repositories = [
        {
          path = "/srv/pool1/backups/myfolder";
          timerConfig = {
            OnCalendar = "00:00:00";
            RandomizedDelaySec = "3h";
          };
        }
      ];

      sourceDirectories = [
        "/var/lib/myfolder"
      ];

      retention = {
        keep_within = "1d";
        keep_hourly = 24;
        keep_daily = 7;
        keep_weekly = 4;
        keep_monthly = 6;
      };

      consistency = {
        repository = "2 weeks";
        archives = "1 month";
      };
    };
  }
