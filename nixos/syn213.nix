{
  config,
  pkgs,
  lib,
  ...
}: {
  fileSystems."/mnt/tmp/Backup" = {
    device = "192.168.1.242:/volume1/Backup";
    options = ["x-systemd.automount" "noauto" "x-systemd.idle-timeout=600"];
    fsType = "nfs";
  };

  fileSystems."/mnt/tmp/TV" = {
    device = "192.168.1.242:/volume1/TV";
    options = ["x-systemd.automount" "noauto" "x-systemd.idle-timeout=600"];
    fsType = "nfs";
  };

  fileSystems."/mnt/tmp/Photos" = {
    device = "192.168.1.242:/volume1/Photos";
    options = ["x-systemd.automount" "noauto" "x-systemd.idle-timeout=600"];
    fsType = "nfs";
  };

  fileSystems."/mnt/tmp/Other" = {
    device = "192.168.1.242:/volume1/Other";
    options = ["x-systemd.automount" "noauto" "x-systemd.idle-timeout=600"];
    fsType = "nfs";
  };

  fileSystems."/mnt/tmp/Music" = {
    device = "192.168.1.242:/volume1/Music";
    options = ["x-systemd.automount" "noauto" "x-systemd.idle-timeout=600"];
    fsType = "nfs";
  };
  fileSystems."/mnt/tmp/Books" = {
    device = "192.168.1.242:/volume1/Books";
    options = ["x-systemd.automount" "noauto" "x-systemd.idle-timeout=600"];
    fsType = "nfs";
  };
  fileSystems."/mnt/tmp/Movies" = {
    device = "192.168.1.242:/volume1/Movies";
    options = ["x-systemd.automount" "noauto" "x-systemd.idle-timeout=600"];
    fsType = "nfs";
  };
}
# breakpoint
# fileSystems."/mnt/tomoyo" = {
# device = "192.168.1.242:/$";
# options = [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600"];
# fsType = "nfs";
# };
# vol
# /volume1/Movies *
# /volume1/Books  *
# /volume1/Backup *
# fileSystems."/export/mafuyu" = {
# device = "/mnt/mafuyu";
# options = [ "bind" ];
# # };

