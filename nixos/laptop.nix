{
  pkgs,
  lib,
  ...
}: {
  home.packages = with pkgs; [
    xorg.xbacklight
  ];
}
