{pkgs, ...}: {
  networking.wireless.userControlled.enable = true;
  environment.systemPackages = with pkgs; [
    wpa_supplicant_gui
    wpa_supplicant
    networkmanagerapplet
    # nm-applet
  ];

  # NetworkManager can be used with several front ends, such as nmcli, nmtui, or nm-applet and nm-connection-editor.
  # networking.networkmanager.enable = true;

  # wpa_cli -g /run/wpa_supplicant/wlp3s0
  # list_network
  # select_network 2

  # build with no wifi if you are in a bind
  # nixos-rebuild switch --option substitute false

  #Here is what I have found works for forcing the captive portal (i.e., login page for WIFI connections on public hotspots):
  #
  #Make the connection to the WIFI.
  #
  #Open a terminal and type route -n. You should receive a display such as:
  #
  #Kernel IP routing table
  #Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
  #default         10.1.0.1        0.0.0.0         UG    600    0        0 wlo1
  #10.1.0.0        *               255.255.248.0   U     600    0        0 wlo1
  #link-local      *               255.255.0.0     U     1000   0        0 wlo1```
  #
  #Type the default Gateway (i.e., 10.1.0.1) into your browser's address bar, you will receive the WIFI login page.
}
