{
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    libreoffice
    pandoc # document converter
    texlive.combined.scheme-full
    treesheets
    meli
    mdp
    visidata
    zettlr
    presenterm
  ];
}
