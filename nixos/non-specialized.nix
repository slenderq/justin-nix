({
  lib,
  flake,
  config,
  pkgs,
  ...
}: {
  config = lib.mkIf (config.specialisation != {}) {
    home-manager.users.justin = {
      home.file.".background-image".source = ../nixos/gui/backgrounds/jimmy-dean-fruit-unsplash.jpg;
    };
  };
})
