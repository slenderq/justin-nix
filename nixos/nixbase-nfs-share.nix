{
  config,
  pkgs,
  lib,
  ...
}: let
  imports = [
    ./backup-montior.nix
  ];
in
  lib.mkIf (config.networking.hostName != "nixbase") {
    fileSystems."/mnt/nixbase" = {
      device = "nixbase:/";
      fsType = "nfs";
      options = ["defaults" "soft" "bg" "auto" "x-systemd.automount" "x-systemd.idle-timeout=30"];
    };
    services.rpcbind.enable = true; # needed for NFS

    # Make sure nfs share are working properly
    services.logind.extraConfig = ''
      KillUserProcess=Yes
      KillExcludeUsers=root
      RemoveIPC=yes
    '';
  }
