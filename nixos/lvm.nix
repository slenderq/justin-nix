{
  config,
  lib,
  pkgs,
  ...
}: {
  # Enable LVM
  boot.initrd.lvm.enable = true;
}
