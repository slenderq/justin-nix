# https://nixos.wiki/wiki/Distributed_build
{
  config,
  lib,
  ...
}:
lib.mkIf (config.networking.hostName != "nixbase") {
  nix.buildMachines = [
    {
      hostName = "nixbase.hedgehog-augmented.ts.net";
      system = "x86_64-linux";
      # if the builder supports building for multiple architectures,
      # replace the previous line by, e.g.,
      # systems = ["x86_64-linux" "aarch64-linux"];
      maxJobs = 32;
      speedFactor = 3;
      supportedFeatures = ["nixos-test" "benchmark" "big-parallel" "kvm"];
      # defined in inject-secrets
      sshKey = "/etc/ssh/nixbase_id_ecdsa";
    }
    #    {
    #      hostName = "nixexit";
    #      system = "x86_64-linux";
    #      maxJobs = 16;
    #      speedFactor = 1;
    #      supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
    #      # defined in inject-secrets
    #      sshKey = "/home/justin/.ssh/id_ed25519";
    #    }
    #
  ];

  # ssh-keygen -y -f /path/to/private_key > /path/to/public_key
  environment.etc = {
    "nixbase_id_ed25519.pub" = {
      text = ''
        ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAi0v4eHGCPZUAfPLWYOMVzRenZO3HeBnUrfpq1EYs32 justin@nixbase
      '';
      # The UNIX file mode bits
      mode = "0644";
    };
  };

  nix.distributedBuilds = true;
  # optional, useful when the builder has a faster internet connection than yours
  nix.extraOptions = ''
    builders-use-substitutes = true
  '';
}
