{
  config,
  pkgs,
  lib,
  ...
}:
lib.mkIf (config.networking.hostName != "nixbase") {
  # https://github.com/ibizaman/selfhostblocks/issues/24
  # need this issue done
  # if we have the share.... backup to it.
  services.borgbackup.jobs = let
    common-excludes = [
      # Largest cache dirs
      ".cache"
      "*/cache2" # firefox
      "*/Cache"
      ".config/Slack/logs"
      ".config/Code/CachedData"
      ".container-diff"
      ".npm/_cacache"
      # Work related dirs
      "*/node_modules"
      "*/bower_components"
      "*/_build"
      "*/.tox"
      "*/venv"
      "*/.venv"
      "**/Steam"
    ];
  in {
    ${config.networking.hostName} = {
      compression = "auto,zstd";
      encryption.mode = "none";
      paths = "/home/justin";
      # https://borgbackup.readthedocs.io/en/stable/usage/general.html
      # defined in inject-secrets.nix
      environment.BORG_RSH = "ssh -i /etc/ssh/ssh_borg_eded25519_key_agenix";
      environment.BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK = "yes";
      environment.BORG_RELOCATED_REPO_ACCESS_IS_OK = "yes";
      # TODO: replace with BORG_KEY_FILE
      environment.BORG_PASSPHRASE = "$(cat ${config.age.secrets.repopass.path})";
      extraCreateArgs = "--verbose --stats --checkpoint-interval 600";
      repo = "justin@nixbase:/mnt/backup/hosts/${config.networking.hostName}";
      startAt = "hourly";
      user = "justin";
      exclude = common-excludes;
      # doInit does not seem to work.
      # Perform `borg init <hostname> -e none` on new hosts and ensure the repo is owned by the 'justin' user.
      doInit = false;
    };
  };

  # ssh-keygen -y -f /path/to/private_key > /path/to/public_key
  environment.etc = {
    "ssh_borg_eded25519_key_agenix.pub" = {
      text = ''
        ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGXBiRKrDctteddgWyJLLF2NRv3MozhIM52DxuslbiFq root@nixbase
      '';
      # The UNIX file mode bits
      mode = "0644";
    };
  };

  # extra-drive-important = basicBorgJob "backups/station/extra-drive-important" // rec {

  #   paths = "/media/extra-drive/important";
  #   exclude = map (x: paths + "/" + x) common-excludes;
  # };
}
