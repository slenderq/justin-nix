{
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    flac
    ffmpeg
    ffmpegthumbnailer
    gimp
    vlc
    smplayer
    mpv
    ciano
    plexamp
    # youtube-dl # insecure in current nixpkgs
    yt-dlp
    # tartube
    pinta
    pick-colour-picker
    cava
  ];
  services.printing.enable = true;
}
