{pkgs, ...}: {
  users.users."justin" = {
    packages = with pkgs; [
      borgbackup
    ];
    extraGroups = ["borg"];
    openssh.authorizedKeys.keys = [
      # borg key public key
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAi0v4eHGCPZUAfPLWYOMVzRenZO3HeBnUrfpq1EYs32 justin"
    ];
  };
}
