{
  config,
  pkgs,
  ...
}: {
  # FIXME: port the pythin script to nix and write the file needed here.
  systemd.services.update-build-sym = {
    description = "update build-symlink";

    wantedBy = ["multi-user.target"];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    #  # have the job run this shell script
    script = with pkgs; ''
      ${pkgs.python310}/bin/python3 /home/justin/bin/python/create_build_sym.py
    '';
  };
}
