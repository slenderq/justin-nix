{
  pkgs,
  lib,
  ...
}: {
  # assuming unfree packages are turned on
  environment.systemPackages = with pkgs; [
    zoom-us
    signal-desktop
    telegram-desktop
  ];
}
