{
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    nixos-generators
    (writeShellApplication {
      name = "create-iso";
      text = ''
        #!/usr/bin/env bash
        nix build .#nixosConfigurations.nixvirt.config.formats.install-iso -o justin-nix-symlink.iso
        cp justin-nix-symlink.iso justin-nix.iso
      '';
    })
    (writeShellApplication {
      name = "create-proxmox";
      text = ''
        #!/usr/bin/env bash
        nix build .#nixosConfigurations.nixvirt.config.formats.proxmox

      '';
    })
  ];
}
