{config, ...}: {
  environment.sessionVariables = {
    # note that this is an antipattern
    # this will cause cleartext to be put in the nix store
    # i'm ok with that risk as it needs to be a common env which is going to get leaked
    OPENAI_API_KEY = "$(cat ${config.age.secrets.openai.path})";
    ANTHROPIC_API_KEY = "$(cat ${config.age.secrets.anthropic.path})";
  };
}
