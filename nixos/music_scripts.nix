{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    (writeShellApplication {
      name = "save_alsa_jack";
      runtimeInputs = [];
      text = ''
        #!/usr/bin/env bash
        aj-snapshot justin-nix/nixos/music_assets/default.aj
        alsactl --file /home/justin/justin-nix/nixos/music_assets/asound.state store
      '';
    })
  ];
}
