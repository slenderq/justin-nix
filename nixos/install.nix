{
  pkgs,
  lib,
  nixos-generators,
  nixpkgs,
  ...
}: {
  modules = [
    nixos-generators.nixosModules.all-formats
    "${toString nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-base.nix"
  ];
  environment.systemPackages = with pkgs; [
    acpi
    bind.dnsutils
    chntpw
    cryptsetup
    efibootmgr
    flashrom
    hwinfo
    inxi
    libsecret
    libva-utils
    lm_sensors
    memtest86-efi
    nixos-generators
    pciutils
    rustdesk
    speedcrunch
    tcpdump
    tdesktop
    usbutils
  ];
}
