{
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    kcc
    calibre
    zathura
    xournal
    masterpdfeditor4
  ];
}
