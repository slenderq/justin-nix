{
  home-manager,
  agenix,
  config,
  pkgs,
  lib,
  codecompanion,
  gen-nvim,
  comfortable-motion,
  numbers,
  vim-scimark,
  telescope-emoji-nvim,
  emojinvim,
  gruvbox,
  cellular-automaton,
  ...
}: {
  imports = [
    home-manager.nixosModules.home-manager
    agenix.nixosModules.default
    ./nix.nix
    ./custom-terminal.nix
    ./ssh-authorize.nix
    ./current-location.nix
    ./nixos-shared.nix
    ./nixbase-nfs-share.nix
    # ./gsettings-fix.nix
    # ./gui/gnome.nix
    ./tailscale.nix
    ./inject-secrets.nix
    ./sudo-pwfeedback.nix
    # ./cache-server.nix
    ./ollama.nix
    ./distributed-build.nix
    ./ai.nix
    ./thunderbird.nix
    # TODO: figure out the self replicating ipxe server
    # ./nixpixie-ssh.nix
    # ./netbootxyz.nix
    ./borgclient.nix
    ./low-battery-hibernate.nix
    ./bootstrap.nix
  ];

  environment.systemPackages = [
    agenix.packages."x86_64-linux".default
  ];

  home-manager.useGlobalPkgs = true;

  users.users."justin".isNormalUser = true;
  home-manager = {
    # backup existing files
    backupFileExtension = "backup";
    # add more options to the {...} top level nix args
    # this is mostly for my neovim pacakges
    extraSpecialArgs = {
      codecompanion = codecompanion;
      gen-nvim = gen-nvim;
      comfortable-motion = comfortable-motion;
      numbers = numbers;
      vim-scimark = vim-scimark;
      telescope-emoji-nvim = telescope-emoji-nvim;
      emojinvim = emojinvim;
      gruvbox = gruvbox;
      cellular-automaton = cellular-automaton;
    };
    users."justin" = {
      home.stateVersion = "23.11";
      imports = [
        ../home/xresources.nix
        ../home/i3.nix
        ../home/kitty.nix
        ../home/git.nix
        ../home/terminal.nix
        ../home/neovim.nix
        ../home/neovim/telescope.nix
        ../home/neovim/coc.nix
        ../home/neovim/unpackaged.nix
        ../home/neovim/llm.nix
        ../home/fish.nix
        ../home/tmux.nix
        # ./starship.nix # close
        # ./nushell.nix
        # ./custom-terminal.nix
      ];
    };
  };
}
