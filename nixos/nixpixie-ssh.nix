{
  config,
  inputs,
  lib,
  pkgs,
  ...
}:
# https://carjorvaz.com/posts/ipxe-booting-with-nixos/
let
  sys = inputs.nixos.lib.nixosSystem {
    system = "x86_64-linux";
    # FIXME: make this a real module file
    modules = [
      ({
        config,
        pkgs,
        lib,
        modulesPath,
        ...
      }: {
        imports = [(modulesPath + "/installer/netboot/netboot-minimal.nix")];
        config = {
          nix.settings.experimental-features = ["nix-command" "flakes"];
          services.openssh = {
            enable = true;
            openFirewall = true;

            settings = {
              PasswordAuthentication = false;
              KbdInteractiveAuthentication = false;
            };
          };

          # https://github.com/nix-community/nixos-anywhere/blob/main/docs/quickstart.md
          # as root on target machine
          # curl -L https://github.com/nix-community/nixos-images/releases/download/nixos-unstable/nixos-kexec-installer-noninteractive-x86_64-linux.tar.gz | tar -xzf- -C /root
          # /root/kexec/run

          users.users.root.openssh.authorizedKeys.keys = [
            # cat .ssh/id_rsa.pub
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC/4Wq6OwiaW0eWRsb1HUu/HCgeiLyIjVCOSE3V2646zs29Y9Oq+WjTbj7aP5O0cvCXCslw0FqI4tD9QKzEg22ZIvuK7Y7IOyhXPq8p2Qq8ZLrOJ6ChNDDrxy4QvdurCpEioj8vvmAMxL1LfzaruLK0oFv2W0+hro69tH/vZ/vSm6cvxqe64rDuHWcK/lZ7Qux6RzKj9J59e4OEo5vkVMAglkFvGhAEDa6f0OZiOFn2XYB0Fqoi58mIQ27b9OfcJV/PHqZq9aVdIBU6Tn8qpbOJ/LhemB0vaRHG5brv9GUJZHnDIqhxLRBTgz+lJglBa1tNZiV04mlt7taT5CR+IqB6UiHs5Fp4GbHYKyO4UwqmPcCwnrKK95t3dudFYqmlSab1vRIX1NC6LbqdG7GmTg9l1jkOrPPMR/y+DTco/hBZWaMLnaWGB6vPsl2z2V/8qfoIfJijOSo10Ou9s4o0ZL8nHm/KyY8hX526fS3IhCAkXMaG/kJKTRMriuSUK1fY2WE= justin@manjaropad"
          ];
        };
      })
    ];
  };

  build = sys.config.system.build;
in {
  services.pixiecore = {
    enable = true;
    openFirewall = true;
    dhcpNoBind = true; # Use existing DHCP server.

    mode = "boot";
    kernel = "${build.kernel}/bzImage";
    initrd = "${build.netbootRamdisk}/initrd";
    cmdLine = "init=${build.toplevel}/init loglevel=4";
    debug = true;
  };
}
