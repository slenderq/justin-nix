{
  pkgs,
  nixpkgs-unstable,
  system,
  config,
  ...
}: let
  pkgs = import nixpkgs-unstable {
    system = "x86_64-linux";
    config = {allowUnfree = true;};
  };
in {
  # https://wiki.nixos.org/wiki/Ente_Photos

  services = {
    immich.enable = true;
    immich.port = 2283;
    # immich.accelerationDevices = null;
    # Photoprism
    photoprism = {
      enable = true;
      port = 2342;
      originalsPath = "/var/lib/private/photoprism/originals";
      address = "0.0.0.0";
      settings = {
        PHOTOPRISM_ADMIN_USER = "admin";
        PHOTOPRISM_ADMIN_PASSWORD = "admin";
        PHOTOPRISM_DEFAULT_LOCALE = "en";
        PHOTOPRISM_DATABASE_DRIVER = "mysql";
        PHOTOPRISM_DATABASE_NAME = "photoprism";
        PHOTOPRISM_DATABASE_SERVER = "/run/mysqld/mysqld.sock";
        PHOTOPRISM_DATABASE_USER = "photoprism";
        PHOTOPRISM_SITE_URL = "${config.networking.hostName}.hedgehog-augmented.ts.net";
        PHOTOPRISM_SITE_TITLE = "My PhotoPrism";
      };
    };
    mysql = {
      enable = true;
      dataDir = "/data/mysql";
      package = pkgs.mariadb;
      ensureDatabases = ["photoprism"];
      ensureUsers = [
        {
          name = "photoprism";
          ensurePermissions = {
            "photoprism.*" = "ALL PRIVILEGES";
          };
        }
      ];
    };
    # NGINX
    nginx = {
      enable = true;
      recommendedTlsSettings = true;
      recommendedOptimisation = true;
      recommendedGzipSettings = true;
      recommendedProxySettings = true;
      clientMaxBodySize = "500m";
      virtualHosts = {
        "${config.networking.hostName}.hedgehog-augmented.ts.net" = {
          forceSSL = true;
          enableACME = true;
          http2 = true;
          locations."/" = {
            proxyPass = "http://127.0.0.1:2342";
            proxyWebsockets = true;
            extraConfig = ''
              proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
              proxy_set_header Host $host;
              proxy_buffering off;
              proxy_http_version 1.1;
            '';
          };
        };
      };
    };
  };

  hardware.opengl.enable = true;
  users.users.immich.extraGroups = ["video" "render"];

  fileSystems."/var/lib/private/photoprism" = {
    device = "/mnt/personal/immich";
    options = ["bind"];
  };

  security.acme.defaults.email = "justin.quaint@protonmail.com";
  security.acme.acceptTerms = true;
}
