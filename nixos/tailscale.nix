{
  config,
  pkgs,
  ...
}: {
  services.tailscale.enable = true;
  networking.firewall.checkReversePath = "loose"; # from warning...
  # https://nixos.wiki/wiki/Tailscale
  services.tailscale.useRoutingFeatures = "both";

  # exit nodes
  boot.kernel.sysctl."net.ipv6.conf.all.forwarding" = 1;
  boot.kernel.sysctl."net.ipv4.ip_forward" = 1;

  networking.firewall = {
    # enable the firewall
    enable = true;

    # always allow traffic from your Tailscale network
    trustedInterfaces = ["tailscale0"];

    # allow the Tailscale UDP port through the firewall
    allowedUDPPorts = [config.services.tailscale.port];
  };

  # make sure resolved is working
  networking.nameservers = ["1.1.1.1#one.one.one.one" "1.0.0.1#one.one.one.one" "194.242.2.2" "100.100.100.100"];

  services.resolved = {
    enable = true;
    dnssec = "true";
    domains = ["~."];
    fallbackDns = ["1.1.1.1#one.one.one.one" "1.0.0.1#one.one.one.one" "100.100.100.100"];
    dnsovertls = "true";
  };

  systemd.services.tailscale-autoconnect = {
    enable = true;
    description = "Automatic connection to Tailscale";

    # make sure tailscale is running before trying to connect to tailscale
    after = ["network-pre.target" "tailscale.service"];
    wants = ["network-pre.target" "tailscale.service"];
    wantedBy = ["multi-user.target"];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    # sudo tailscale up --exit-node=ca-yyc-wg-201.mullvad.ts.net  --exit-node-allow-lan-access=true --accept-routes=true
    # have the job run this shell script
    script = with pkgs; ''
      # wait for tailscaled to settle
      sleep 2

      # check if we are already authenticated to tailscale
      status="$(${tailscale}/bin/tailscale status -json | ${jq}/bin/jq -r .BackendState)"
      if [ $status = "Running" ]; then # if so, then do nothing
        exit 0
      fi

      # otherwise authenticate with tailscale
      echo "authkey is needed as a secret get one from: https://login.tailscale.com/admin/settings/keys and update tailscale.nix"
      ${tailscale}/bin/tailscale up -authkey file:${config.age.secrets.tailscale.path}
    '';
  };
}
