{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    (writeShellApplication {
      name = "fast-reboot";

      text = ''
        #!/usr/bin/env bash
        sudo systemctl kexec
      '';
    })
    (writeShellApplication {
      name = "audio_info";

      text = ''
        #!/usr/bin/env bash
        inxi --audio --admin --filter --width 80
      '';
    })
    (writeShellApplication {
      name = "backupfile";

      text = ''
        #!/usr/bin/env bash

        file="$1"

        sudo cp "$file" "''${file}.old"
      '';
    })
    (writeShellApplication {
      name = "bespoke";

      text = ''
        #!/usr/bin/env bash
        #nix-env -iPA nixpkgs.bespokesynth-with-vst2 nixpkgs.yabridge nixpkgs.winePackages.staging
        #nix-env -iPA nixpkgs.bespokesynth nixpkgs.yabridge  nixpkgs.winePackages.staging
        sudo nice -n -10 BespokeSynth
      '';
    })
    (writeShellApplication {
      name = "build-nix";

      text = ''
        #!/usr/bin/env bash
        cd ~/justin-nix
        sudo nix run
      '';
    })

    (writeShellApplication {
      name = "convert_all_to_flac";
      runtimeInputs = [pkgs.ffmpeg];
      text = ''
        pattern=''${1:-"*.mp3/i"}
        for i in $pattern; do ffmpeg -i "$i" -c:a flac "''${i%.*}.flac"; done
      '';
    })
    (writeShellApplication {
      name = "edit-diff";

      text = ''
        #!/usr/bin/env bash
        nvim -p "$(git diff --name-only origin/main)"
      '';
    })
    (writeShellApplication {
      name = "edit-fish";

      text = ''
        #!/usr/bin/env bash
        nvim ~/justin-shell/home/fish.nix
      '';
    })
    (writeShellApplication {
      name = "edit-i3";

      text = ''
        #!/usr/bin/env bash
        nvim -c ":set filetype=python" -p ~/justin-nix/home/i3/config ~/justin-nix/home/i3status/config ~/justin-nix/home/i3/*.nix ~/justin-nix/i3.nix
      '';
    })
    (writeShellApplication {
      name = "edit-nixconf";

      text = ''
        #!/usr/bin/env bash
        nvim -p "$HOME/justin-nix/systems/$(hostname).nix" ~/justin-nix/flake.nix ~/justin-nix/
        build-nix
      '';
    })
    (writeShellApplication {
      name = "edit-nu";

      text = ''
        #!/usr/bin/env bash
        nvim ~/justin-nix/home/nushell.nix
      '';
    })
    (writeShellApplication {
      name = "edit-starship";

      text = ''
        #!/usr/bin/env bash
        nvim -p ~/justin-nix/home/starship.nix
      '';
    })
    (writeShellApplication {
      name = "edit-vim";

      text = ''
        #!/usr/bin/env bash
        nvim -p ~/justin-nix/home/neovim.nix ~/justin-nix/home/neovim/*.nix
      '';
    })
    (writeShellApplication {
      name = "empty_trash";

      text = ''
        #!/usr/bin/env bash
        rm -R "$HOME"/.local/share/Trash/files/*
        rm -R "$HOME"/.local/share/Trash/info/*
      '';
    })
    (writeShellApplication {
      name = "extend-volume";

      text = ''
        #!/usr/bin/env bash
        echo
        echo Please check if you have room for the volume...
        sudo vgs
        echo

        echo What Volume Would you like to extend?
        sudo lvs

        echo e.g. /dev/HDDVolGroup/movies

        read -r vol
        echo

        echo
        echo How much space should be added?
        echo e.g. +50G
        read -r amount

        echo
        echo "Increase $vol by $amount ?"

        echo
        echo Is the new volume listed?
        read -r answer

        if [ "$answer" = "n" ] ; then
           exit 1
        fi
        sudo lvextend -L "$amount" "$vol"
        sudo resize2fs "$vol"
      '';
    })
    (writeShellApplication {
      name = "flush_dhcp";

      text = ''
        sudo dhclient -r
        sudo dhclient
      '';
    })
    (writeShellApplication {
      name = "get_last_boot_message";

      text = ''
        sudo journalctl -o short-precise -b -1
      '';
    })
    (writeShellApplication {
      name = "rn_work";

      text = ''
        #! /usr/bin/env bash
        i3-msg rename workspace to "$1"
      '';
    })
    (writeShellApplication {
      name = "journal";

      text = ''
        #!/usr/bin/env bash
        nb journal:add
        nb journal:sync
      '';
    })
    (writeShellApplication {
      name = "kernal_buffer";

      text = ''
        #!/usr/bin/env bash
        sudo dmesg -T --color=always | bat
      '';
    })
    (writeShellApplication {
      name = "move_and_convert_from_card";

      text = ''
        #!/usr/bin/env bash
        mount_point="$1/STEREO/FOLDER01"
        storage_point="$2"
        ls "$1"
        ls "$2"

        date=$(date "+%Y%m%d")
        for i in "$mount_point"/*; do
          filename=$(basename -- "$i")

          ffmpeg -i "$i" -c:a flac "$storage_point"/"''${filename}"_"''${date}".flac

        done

        raw_point="$storage_point/.raw"
        mkdir -p "$raw_point"

        mv "$mount_point"/* "$raw_point"/
      '';
    })
    (writeShellApplication {
      name = "mvln";

      text = ''
        #! /usr/bin/env bash
        mv "$1" "$2" && ln -s "$2" "$1"
      '';
    })
    (writeShellApplication {
      name = "prs";

      text = ''
        #!/usr/bin/env bash
        gh pr status
        gh pr list
      '';
    })
    (writeShellApplication {
      name = "turn_off_sd_reader";

      text = ''
        echo 2-3 > /sys/bus/usb/drivers/usb/unbind
      '';
    })
    (writeShellApplication {
      name = "turn_off_sd_reader";

      text = ''
        echo 2-3 > /sys/bus/usb/drivers/usb/unbind
      '';
    })
    (writeShellApplication {
      name = "unlock_user";

      text = ''
        passwd -u justin
      '';
    })
    (writeShellApplication {
      name = "vish";

      text = ''
        nvim ~/justin-nix/home/custom-terminal.nix
      '';
    })
    (writeShellApplication {
      name = "yabridge_sync";

      text = ''
        HOME/.local/share/yabridge/yabridgectl sync
      '';
    })
    (writeShellApplication {
      name = "last-shutdown-log";
      text = ''
        sudo journalctl -b -1 -e
      '';
    })
    (writeShellApplication {
      name = "nb-remotes-add";
      text = ''
        nb home:remote set git@gitlab.com:slenderq/nb-notes.git
        nb home:notebooks new journal
        nb journal:remote set git@gitlab.com:slenderq/nb-journal.git
      '';
    })
    (writeShellApplication {
      name = "themes";
      text = ''
        kitty +kitten themes
      '';
    })
    (
      writeShellApplication {
        name = ",repair-auto-optimized-nix-store";
        text = ''
          nix-store --verify --check-contents --repair
        '';
      }
    )
  ];
}
