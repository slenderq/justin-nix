{
  pkgs,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    rnnoise-plugin
    rnnoise
    # tonelib-noisereducer
    speech-denoiser
    # jalv
    # carla
  ];
  services.printing.enable = true;
}
