{pkgs, ...}: {
  environment.systemPackages = with pkgs; [
    ollama
  ];

  systemd.user.services.ollama = {
    enable = true;
    description = "Ollama Service";
    serviceConfig = {
      ExecStart = "${pkgs.ollama}/bin/ollama serve";
      Restart = "on-failure";
    };
    wantedBy = ["multi-user.target"]; # starts after login
  };
}
