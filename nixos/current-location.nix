{
  time.timeZone = "America/Edmonton";

  location = {
    provider = "manual";
    # Calgary
    latitude = 51.048615;
    longitude = -114.070847;
  };
}
