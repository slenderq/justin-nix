# https://lazamar.co.uk/nix-versions/
# { config, pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/824421b1796332ad1bcb35bc7855da832c43305f.tar.gz"), ... }:
{
  config,
  pkgs,
  ...
}: {
  # symlinkJoin { name = "myexample"; paths = [ pkgs.hello pkgs.stack ]; postBuild = "echo links added"; }
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
    package = pkgs.steam.override {
      extraPkgs = pkgs: [];
      # withPrimus = true;
    };
  };

  # programs.steam.gamescopeSession.enable = true;
  # https://github.com/NixOS/nixpkgs/issues/47932#issuecomment-447508411

  hardware.graphics = {
    enable = true;
    enable32Bit = true;
  };

  # hardware.steam-hardware.enable = true;

  environment.systemPackages = with pkgs; [
    steamcmd
    steam-tui
    glxinfo
    pcsx2 # Playstation 2
    # xbox drivers
    xboxdrv
    mesa
    retroarch-joypad-autoconfig
    # unstable.retroarchFull
    (retroarch.override {
      cores = [
        libretro.genesis-plus-gx
        libretro.snes9x
        libretro.beetle-psx-hw
        libretro.pcsx-rearmed
        libretro.nestopia
        libretro.mame
        libretro.genesis-plus-gx
        libretro.dolphin
        libretro.swanstation
        libretro.snes9x
        libretro.quicknes
        libretro.scummvm
        # libretro.parallel-n64
        # libretro.sameboy
        #libretro.stella
      ];
    })
    eidolon # game registry
    xemu # Original Xbox
    # copywrite might want to use old version via nixpkgs?
    # yuzu-mainline # Nintendo Switch
    duckstation # Playstation 1
    discord
  ];
  # duckstation settings.
  # /home/justin/.config/duckstation/settings.ini
  home-manager.users."justin" = {
    xdg = {
      configFile = {
        "duckstation" = {
          source = ./duckstation;
          target = "duckstation";
          recursive = true;
        };
      };
    };
  };
}
