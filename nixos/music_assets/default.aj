<?xml version="1.0" encoding="utf-8"?>
<aj-snapshot>
<alsa>
  <client name="System">
    <port id="0" />
    <port id="1" />
  </client>
  <client name="Midi Through">
    <port id="0" />
  </client>
  <client name="OSS sequencer" />
  <client name="SB Audigy 5/Rx [SB1550]">
    <port id="0" />
    <port id="32" />
  </client>
  <client name="Emu10k1 WaveTable" />
  <client name="Arturia MicroFreak">
    <port id="0" />
  </client>
  <client name="MPK mini 3">
    <port id="0" />
  </client>
  <client name="Client-128" />
  <client name="a2jmidid" />
  <client name="sooperlooper">
    <port id="0" />
  </client>
  <client name="BespokeSynth" />
  <client name="aj-snapshot" />
</alsa>
<jack>
  <client name="system">
    <port name="capture_1">
      <connection port="sooperlooper:common_in_1" />
      <connection port="BespokeSynth:in_1" />
    </port>
    <port name="capture_2">
      <connection port="sooperlooper:common_in_2" />
      <connection port="BespokeSynth:in_2" />
    </port>
    <port name="capture_3">
      <connection port="BespokeSynth:in_3" />
    </port>
    <port name="capture_4">
      <connection port="BespokeSynth:in_4" />
    </port>
    <port name="capture_5">
      <connection port="BespokeSynth:in_5" />
    </port>
    <port name="capture_6">
      <connection port="BespokeSynth:in_6" />
    </port>
    <port name="capture_7">
      <connection port="BespokeSynth:in_7" />
    </port>
    <port name="capture_8">
      <connection port="BespokeSynth:in_8" />
    </port>
    <port name="capture_9">
      <connection port="BespokeSynth:in_9" />
    </port>
    <port name="capture_10">
      <connection port="BespokeSynth:in_10" />
    </port>
    <port name="capture_11">
      <connection port="sooperlooper:common_in_1" />
      <connection port="BespokeSynth:in_11" />
    </port>
    <port name="capture_12">
      <connection port="sooperlooper:common_in_2" />
      <connection port="BespokeSynth:in_12" />
    </port>
    <port name="capture_13">
      <connection port="BespokeSynth:in_13" />
    </port>
    <port name="capture_14">
      <connection port="BespokeSynth:in_14" />
    </port>
    <port name="capture_15">
      <connection port="BespokeSynth:in_15" />
    </port>
    <port name="capture_16">
      <connection port="BespokeSynth:in_16" />
    </port>
  </client>
  <client name="a2j">
    <port name="Midi Through (capture): Midi Through Port-0" />
    <port name="SB Audigy 5/Rx [SB1550] (capture): Audigy MPU-401 (UART)" />
    <port name="SB Audigy 5/Rx [SB1550] (capture): Audigy MPU-401  2" />
    <port
name="Arturia MicroFreak (capture): Arturia MicroFreak Arturia Micr" />
    <port name="MPK mini 3 (capture): MPK mini 3 MIDI 1">
      <connection port="a2j:sooperlooper (playback): sooperlooper" />
    </port>
  </client>
  <client name="sooperlooper">
    <port name="common_out_1">
      <connection port="system:playback_1" />
      <connection port="BespokeSynth:in_1" />
    </port>
    <port name="common_out_2">
      <connection port="system:playback_2" />
      <connection port="BespokeSynth:in_2" />
    </port>
    <port name="loop0_out_1" />
    <port name="loop0_out_2" />
  </client>
  <client name="a2j">
    <port name="sooperlooper (capture): sooperlooper" />
  </client>
  <client name="BespokeSynth">
    <port name="out_1">
      <connection port="system:playback_1" />
    </port>
    <port name="out_2">
      <connection port="system:playback_2" />
    </port>
    <port name="out_3">
      <connection port="sooperlooper:loop0_in_1" />
    </port>
    <port name="out_4">
      <connection port="sooperlooper:loop0_in_2" />
    </port>
    <port name="out_5">
      <connection port="system:playback_5" />
    </port>
    <port name="out_6">
      <connection port="system:playback_6" />
    </port>
    <port name="out_7">
      <connection port="system:playback_7" />
    </port>
    <port name="out_8">
      <connection port="system:playback_8" />
    </port>
    <port name="out_9">
      <connection port="system:playback_9" />
    </port>
    <port name="out_10">
      <connection port="system:playback_10" />
    </port>
    <port name="out_11">
      <connection port="system:playback_11" />
    </port>
    <port name="out_12">
      <connection port="system:playback_12" />
    </port>
    <port name="out_13">
      <connection port="system:playback_13" />
    </port>
    <port name="out_14">
      <connection port="system:playback_14" />
    </port>
    <port name="out_15">
      <connection port="system:playback_15" />
    </port>
    <port name="out_16">
      <connection port="system:playback_16" />
    </port>
  </client>
</jack>
</aj-snapshot>
